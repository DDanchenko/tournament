package main

import (
	"bytes"
	"database/sql"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strconv"
	"testing"

	sqlmock "gopkg.in/DATA-DOG/go-sqlmock.v1"
)

/*
I think mixing http handler tests and db mock is bad idea.
A real app need to divide db presentation and request scenarious.
This way was choosed because I trying to complete it quickly.  (But it has a lot of time too)
*/

func dbMock(t *testing.T) (*sql.DB, sqlmock.Sqlmock, error) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	return db, mock, err
}

func serveHTTP(method string, request string, reader io.Reader, dbactor dbActor, handler func(rw http.ResponseWriter, r *http.Request, dbactor dbActor) (int, error), t *testing.T) *httptest.ResponseRecorder {
	req, err := http.NewRequest(method, request, reader)
	if err != nil {
		t.Fatal(err)
	}

	// We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
	rr := httptest.NewRecorder()

	h := http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
		code, err := handler(rw, r, dbactor)
		rw.WriteHeader(code)
		if err != nil {
			rw.Write([]byte(err.Error()))
			//fmt.Printf("ERROR: %s\n", err.Error())
		}
	})

	// Our handlers satisfy http.Handler, so we can call their ServeHTTP method
	// directly and pass in our Request and ResponseRecorder.
	h.ServeHTTP(rr, req)
	return rr
}

func TestFundPlayerHandler(t *testing.T) {
	johnID := "john"
	saraID := "sara"

	// 1 request
	actor := &dbActorMock{}
	actor.misPlayerExist = func(playerID string) (bool, error) {
		if johnID == playerID {
			return true, nil
		}
		return false, fmt.Errorf("mock error")
	}
	actor.mfundPlayer = func(playerID string, amount uint) error {
		if johnID == playerID && amount == 10100 {
			return nil
		}
		return fmt.Errorf("mock error")
	}

	rr := serveHTTP("GET", "http://localhost:8080/fund?playerId=john&points=10100", nil, actor, fundHandler, t)
	if rr.Code != http.StatusOK || actor.isAllExpectedCall() == false {
		t.Errorf("handler returned wrong status code: got %v want %v",
			rr.Code, http.StatusOK)
	}

	// 2 request
	actor = &dbActorMock{}
	actor.misPlayerExist = func(playerID string) (bool, error) {
		return false, sql.ErrNoRows
	}
	actor.maddPlayer = func(playerID string, amount uint) error {
		if saraID == playerID && amount == 10100 {
			return nil
		}
		return fmt.Errorf("mock error")
	}

	rr = serveHTTP("GET", "http://localhost:8080/fund?playerId=sara&points=10100", nil, actor, fundHandler, t)
	if rr.Code != http.StatusOK || actor.isAllExpectedCall() == false {
		t.Errorf("handler returned wrong status code: got %v want %v",
			rr.Code, http.StatusOK)
	}

	// testing error causing by not integer points:
	rr = serveHTTP("GET", "http://localhost:8080/fund?playerId=sara&points=notInteger", nil, actor, fundHandler, t)
	if rr.Code != http.StatusBadRequest || actor.isAllExpectedCall() == false {
		t.Errorf("handler returned wrong status code: got %v want %v",
			rr.Code, http.StatusBadRequest)
	}

	// 3 request
	actor = &dbActorMock{}
	actor.misPlayerExist = func(playerID string) (bool, error) {
		return false, fmt.Errorf("some database error")
	}

	// 3) testing error causing by some database error:
	rr = serveHTTP("GET", "http://localhost:8080/fund?playerId=sara&points=10100", nil, actor, fundHandler, t)
	if rr.Code != http.StatusInternalServerError || actor.isAllExpectedCall() == false {
		t.Errorf("handler returned wrong status code: got %v want %v",
			rr.Code, http.StatusInternalServerError)
	}

	// 4 request
	actor = &dbActorMock{}
	actor.misPlayerExist = func(playerID string) (bool, error) {
		return true, nil
	}
	actor.mfundPlayer = func(playerID string, amount uint) error {
		return fmt.Errorf("some db error")
	}

	// 4) testing error causing by some database error:
	rr = serveHTTP("GET", "http://localhost:8080/fund?playerId=john&points=10100", nil, actor, fundHandler, t)
	if rr.Code != http.StatusInternalServerError || actor.isAllExpectedCall() == false {
		t.Errorf("handler returned wrong status code: got %v want %v",
			rr.Code, http.StatusInternalServerError)
	}

	// 5 request
	actor = &dbActorMock{}
	actor.misPlayerExist = func(playerID string) (bool, error) {
		return false, sql.ErrNoRows
	}
	actor.maddPlayer = func(playerID string, amount uint) error {
		return fmt.Errorf("some db error")
	}

	// 5) testing adding new player
	rr = serveHTTP("GET", "http://localhost:8080/fund?playerId=sara&points=10100", nil, actor, fundHandler, t)
	if rr.Code != http.StatusInternalServerError || actor.isAllExpectedCall() == false {
		t.Errorf("handler returned wrong status code: got %v want %v",
			rr.Code, http.StatusInternalServerError)
	}

	// testing error causing by absent some parameters:
	rr = serveHTTP("GET", "http://localhost:8080/fund?playerId=sara", nil, actor, fundHandler, t)
	if rr.Code != http.StatusBadRequest {
		t.Errorf("handler returned wrong status code: got %v want %v",
			rr.Code, http.StatusBadRequest)
	}

	// testing absent some parameters error:
	rr = serveHTTP("GET", "http://localhost:8080/fund?playerId=&points=1234", nil, actor, fundHandler, t)
	if rr.Code != http.StatusBadRequest {
		t.Errorf("handler returned wrong status code: got %v want %v",
			rr.Code, http.StatusBadRequest)
	}

	// testing negative points error:
	rr = serveHTTP("GET", "http://localhost:8080/fund?playerId=sara&points=-1234", nil, actor, fundHandler, t)
	if rr.Code != http.StatusBadRequest {
		t.Errorf("handler returned wrong status code: got %v want %v",
			rr.Code, http.StatusBadRequest)
	}
}

func TestTakePlayerHandler(t *testing.T) {
	db, mock, err := dbMock(t)
	if err != nil {
		t.Errorf("can't start dbmock %s", err.Error())
	}
	defer db.Close()

	amount := uint(10100)
	johnID := "john"
	saraID := "sara"

	// 1 request success case
	actor := &dbActorMock{}
	actor.misPlayerExist = func(playerID string) (bool, error) {
		if playerID == johnID {
			return true, nil
		}
		return false, fmt.Errorf("TestTakePlayerHandler mock error request 1")
	}
	actor.mgetPlayerBalance = func(playerID string) (uint, error) {
		if playerID == johnID {
			return 100000, nil
		}
		return 0, fmt.Errorf("TestTakePlayerHandler mock error request 1")
	}
	actor.mtakePlayer = func(playerID string, amount uint) error {
		if playerID == johnID && amount == amount {
			return nil
		}
		return fmt.Errorf("TestTakePlayerHandler mock error request 1")
	}
	rr := serveHTTP("GET", "http://localhost:8080/take?playerId=john&points=10100", nil, actor, takeHandler, t)
	if rr.Code != http.StatusOK || actor.isAllExpectedCall() == false {
		t.Errorf("handler returned wrong status code: got %v want %v",
			rr.Code, http.StatusOK)
	}

	// 2 request testing player absent error (add player with funds)
	mock.ExpectQuery("SELECT points FROM players WHERE playerid=").WithArgs(saraID).WillReturnError(sql.ErrNoRows)
	actor = &dbActorMock{}
	actor.misPlayerExist = func(playerID string) (bool, error) {
		if saraID == playerID {
			return false, sql.ErrNoRows
		}
		return false, fmt.Errorf("TestTakePlayerHandler mock error request 2")
	}
	rr = serveHTTP("GET", "http://localhost:8080/take?playerId=sara&points=10100", nil, actor, takeHandler, t)
	if rr.Code != http.StatusBadRequest || actor.isAllExpectedCall() == false {
		t.Errorf("handler returned wrong status code: got %v want %v",
			rr.Code, http.StatusBadRequest)
	}

	// 3 request isPlayerExist return some db error
	actor = &dbActorMock{}
	actor.misPlayerExist = func(playerID string) (bool, error) {
		if saraID == playerID {
			return false, fmt.Errorf("some db error")
		}
		return false, fmt.Errorf("TestTakePlayerHandler mock error request 3")
	}
	rr = serveHTTP("GET", "http://localhost:8080/take?playerId=sara&points=10100", nil, actor, takeHandler, t)
	if rr.Code != http.StatusInternalServerError || actor.isAllExpectedCall() == false {
		t.Errorf("handler returned wrong status code: got %v want %v",
			rr.Code, http.StatusInternalServerError)
	}

	// 4 request getPlayerBalance return db eror
	actor = &dbActorMock{}
	actor.misPlayerExist = func(playerID string) (bool, error) {
		if playerID == saraID {
			return true, nil
		}
		return false, fmt.Errorf("TestTakePlayerHandler mock error request 4")
	}
	actor.mgetPlayerBalance = func(playerID string) (uint, error) {
		if playerID == saraID {
			return 0, fmt.Errorf("some db error")
		}
		return 0, fmt.Errorf("TestTakePlayerHandler mock error request 4")
	}
	rr = serveHTTP("GET", "http://localhost:8080/take?playerId=sara&points=10100", nil, actor, takeHandler, t)
	if rr.Code != http.StatusInternalServerError || actor.isAllExpectedCall() == false {
		t.Errorf("handler returned wrong status code: got %v want %v",
			rr.Code, http.StatusInternalServerError)
	}

	//5 request takePlayer return db error
	actor = &dbActorMock{}
	actor.misPlayerExist = func(playerID string) (bool, error) {
		if playerID == saraID {
			return true, nil
		}
		return false, fmt.Errorf("TestTakePlayerHandler mock error request 5")
	}
	actor.mgetPlayerBalance = func(playerID string) (uint, error) {
		if playerID == saraID {
			return 100000, nil
		}
		return 0, fmt.Errorf("TestTakePlayerHandler mock error request 5")
	}
	actor.mtakePlayer = func(playerID string, Amount uint) error {
		if playerID == saraID && amount == Amount {
			return fmt.Errorf("some db error")
		}
		return fmt.Errorf("TestTakePlayerHandler mock error request 5")
	}
	rr = serveHTTP("GET", "http://localhost:8080/take?playerId=sara&points=10100", nil, actor, takeHandler, t)
	if rr.Code != http.StatusInternalServerError || actor.isAllExpectedCall() == false {
		t.Errorf("handler returned wrong status code: got %v want %v",
			rr.Code, http.StatusInternalServerError)
	}

	//5 request player does not have much points
	actor = &dbActorMock{}
	actor.misPlayerExist = func(playerID string) (bool, error) {
		if playerID == saraID {
			return true, nil
		}
		return false, fmt.Errorf("TestTakePlayerHandler mock error request 5")
	}
	actor.mgetPlayerBalance = func(playerID string) (uint, error) {
		if playerID == saraID {
			return 100, nil
		}
		return 0, fmt.Errorf("TestTakePlayerHandler mock error request 5")
	}
	rr = serveHTTP("GET", "http://localhost:8080/take?playerId=sara&points=10100", nil, actor, takeHandler, t)
	if rr.Code != http.StatusBadRequest || actor.isAllExpectedCall() == false {
		t.Errorf("handler returned wrong status code: got %v want %v",
			rr.Code, http.StatusBadRequest)
	}

	// 6 request testing error causing by not integer points:
	rr = serveHTTP("GET", "http://localhost:8080/take?playerId=sara&points=notInteger", nil, nil, takeHandler, t)
	if rr.Code != http.StatusBadRequest {
		t.Errorf("handler returned wrong status code: got %v want %v",
			rr.Code, http.StatusBadRequest)
	}

	//7 request testing error causing by absent some parameters:
	rr = serveHTTP("GET", "http://localhost:8080/take?playerId=sara", nil, nil, takeHandler, t)
	if rr.Code != http.StatusBadRequest {
		t.Errorf("handler returned wrong status code: got %v want %v",
			rr.Code, http.StatusBadRequest)
	}

	// 8 request testing error causing by absent some parameters:
	rr = serveHTTP("GET", "http://localhost:8080/take?playerId=&points=10200", nil, nil, takeHandler, t)
	if rr.Code != http.StatusBadRequest {
		t.Errorf("handler returned wrong status code: got %v want %v",
			rr.Code, http.StatusBadRequest)
	}

	// 9 request testing error causing by negative points:
	rr = serveHTTP("GET", "http://localhost:8080/take?playerId=sara&points=-10200", nil, nil, takeHandler, t)
	if rr.Code != http.StatusBadRequest {
		t.Errorf("handler returned wrong status code: got %v want %v",
			rr.Code, http.StatusBadRequest)
	}
}

func TestAnnounceTournamentHandler(t *testing.T) {

	tournamentID := "tournament100500"
	deposit := uint(1000)

	//1 request success case
	actor := &dbActorMock{}
	actor.mtournamentState = func(tournamentId string) (byte, error) {
		return byte(0), sql.ErrNoRows
	}
	actor.mannounceTournament = func(tournamenId string, deposit uint) error {
		if tournamenId == tournamentID {
			return nil
		}
		return fmt.Errorf("TestAnnounceTournamentHandler mock error request 1")
	}
	request := fmt.Sprintf("announceTournament?tournamentId=%s&deposit=%d", tournamentID, deposit)
	rr := serveHTTP("GET", request, nil, actor, announceTournamentHandler, t)
	if rr.Code != http.StatusOK || actor.isAllExpectedCall() == false {
		t.Errorf("handler returned wrong status code: got %v want %v",
			rr.Code, http.StatusOK)
	}

	// 2 request testing player absent error
	actor = &dbActorMock{}
	actor.mtournamentState = func(tournamentId string) (byte, error) {
		if tournamentID == tournamentId {
			return 's', nil
		}
		return byte(0), fmt.Errorf("TestAnnounceTournamentHandler mock error request 2")
	}
	rr = serveHTTP("GET", request, nil, actor, announceTournamentHandler, t)
	if rr.Code != http.StatusBadRequest || actor.isAllExpectedCall() == false {
		t.Errorf("handler returned wrong status code: got %v want %v",
			rr.Code, http.StatusBadRequest)
	}

	// 3 request testing error causing by some database error
	actor = &dbActorMock{}
	actor.mtournamentState = func(tournamentId string) (byte, error) {
		return byte(0), fmt.Errorf("some db error")
	}
	rr = serveHTTP("GET", request, nil, actor, announceTournamentHandler, t)
	if rr.Code != http.StatusInternalServerError || actor.isAllExpectedCall() == false {
		t.Errorf("handler returned wrong status code: got %v want %v",
			rr.Code, http.StatusInternalServerError)
	}

	// 4 request testing error causing by some database error:
	actor = &dbActorMock{}
	actor.mtournamentState = func(tournamentId string) (byte, error) {
		return byte(0), sql.ErrNoRows
	}
	actor.mannounceTournament = func(tournamenId string, deposit uint) error {
		return fmt.Errorf("TestTakePlayerHandler mock error request 4")
	}
	rr = serveHTTP("GET", request, nil, actor, announceTournamentHandler, t)
	if rr.Code != http.StatusInternalServerError || actor.isAllExpectedCall() == false {
		t.Errorf("handler returned wrong status code: got %v want %v",
			rr.Code, http.StatusInternalServerError)
	}

	// 5) testing error causing by not integer deposit:
	request = fmt.Sprintf("announceTournament?tournamentId=%s&deposit=notInteger", tournamentID)
	rr = serveHTTP("GET", request, nil, nil, announceTournamentHandler, t)
	if rr.Code != http.StatusBadRequest {
		t.Errorf("handler returned wrong status code: got %v want %v",
			rr.Code, http.StatusBadRequest)
	}

	// 6) testing error causing by absent some parameters:
	request = fmt.Sprintf("announceTournament?tournamentId=%s&deposit=", tournamentID)
	rr = serveHTTP("GET", request, nil, nil, announceTournamentHandler, t)
	if rr.Code != http.StatusBadRequest {
		t.Errorf("handler returned wrong status code: got %v want %v",
			rr.Code, http.StatusBadRequest)
	}

	// testing error causing by absent some parameters:
	rr = serveHTTP("GET", "announceTournament?tournamentId=&deposit=200", nil, nil, announceTournamentHandler, t)
	if rr.Code != http.StatusBadRequest {
		t.Errorf("handler returned wrong status code: got %v want %v",
			rr.Code, http.StatusBadRequest)
	}

	// testing error causing by negative points:
	request = fmt.Sprintf("announceTournament?tournamentId=%s&deposit=-10500", tournamentID)
	rr = serveHTTP("GET", request, nil, nil, announceTournamentHandler, t)

	if rr.Code != http.StatusBadRequest {
		t.Errorf("handler returned wrong status code: got %v want %v",
			rr.Code, http.StatusBadRequest)
	}
}

func TestJoinTournamentHandler(t *testing.T) {

	tournamentID := "1"
	tournamentDeposit := uint(1000)
	playerID := "P1"
	player1Balance := uint(1500)
	baker1ID := "P2"
	baker1Balance := uint(1500)
	baker2ID := "P3"
	baker2Balance := uint(1500)
	baker3ID := "P4"
	baker3Balance := uint(1500)
	baker4ID := "P5"
	baker4Balance := uint(1500)
	bakersAndPlayers := uint(5)
	stake := uint(tournamentDeposit / uint(bakersAndPlayers))

	// 1) testing success case:
	actor := &dbActorMock{}
	actor.mbeginTransaction = func() (dbTransActor, error) {
		return actor, nil
	}
	actor.mgetTournament = func(tourId string) (uint, string, error) {
		if tourId == tournamentID {
			return tournamentDeposit, "s", nil
		}
		return 0, "", fmt.Errorf("request 1 mock error 0")
	}
	actor.misPlayerExist = func(playerId string) (bool, error) {
		if playerID == playerId {
			return true, nil
		} else if baker1ID == playerId {
			return true, nil
		} else if baker2ID == playerId {
			return true, nil
		} else if baker3ID == playerId {
			return true, nil
		} else if baker4ID == playerId {
			return true, nil
		}
		return false, fmt.Errorf("request 1 mock error 1")
	}
	actor.mgetPlayerBalance = func(playerId string) (uint, error) {
		if playerID == playerId {
			return player1Balance, nil
		} else if baker1ID == playerId {
			return baker1Balance, nil
		} else if baker2ID == playerId {
			return baker2Balance, nil
		} else if baker3ID == playerId {
			return baker3Balance, nil
		} else if baker4ID == playerId {
			return baker4Balance, nil
		}
		return 0, fmt.Errorf("request 1 mock error 2")
	}
	actor.mtakePlayer = func(playerId string, amount uint) error {
		if playerID == playerId {
			return nil
		} else if baker1ID == playerId {
			return nil
		} else if baker2ID == playerId {
			return nil
		} else if baker3ID == playerId {
			return nil
		} else if baker4ID == playerId {
			return nil
		}
		return fmt.Errorf("request 1 mock error 3")
	}
	actor.mjoinTournament = func(tournamentId string, playerId string, joinerType string, bakerId string, Stake uint) error {
		if tournamentId == tournamentID && stake == Stake {
			if playerID == playerId {
				if joinerType == "b" {
					if baker1ID == bakerId {
						return nil
					} else if baker2ID == bakerId {
						return nil
					} else if baker3ID == bakerId {
						return nil
					} else if baker4ID == bakerId {
						return nil
					}
				} else if joinerType == "p" {
					return nil
				}
			}
		}
		return fmt.Errorf("request 1 mock error 4")
	}
	actor.mcommit = func() error {
		return nil
	}
	request1 := fmt.Sprintf("http://localhost:8080/joinTournament?tournamentId=%s&playerId=%s&backerId=%s&backerId=%s&backerId=%s&backerId=%s", tournamentID, playerID, baker1ID, baker2ID, baker3ID, baker4ID)
	rr := serveHTTP("GET", request1, nil, actor, joinTournamentHandler, t)
	if rr.Code != http.StatusOK || actor.isAllExpectedCall() == false {
		t.Errorf("handler returned wrong status code: got %v want %v",
			rr.Code, http.StatusOK)
	}

	// 2) testing case when tournament is closed
	actor = &dbActorMock{}
	actor.mbeginTransaction = func() (dbTransActor, error) {
		return actor, nil
	}
	actor.mgetTournament = func(tourId string) (uint, string, error) {
		if tourId == tournamentID {
			return tournamentDeposit, "c", nil
		}
		return 0, "", fmt.Errorf("request 2 mock error 0")
	}
	actor.mrollback = func() error {
		return nil
	}
	request2 := request1
	rr = serveHTTP("GET", request2, nil, actor, joinTournamentHandler, t)
	if rr.Code != http.StatusBadRequest || actor.isAllExpectedCall() == false {
		t.Errorf("handler returned wrong status code: got %v want %v",
			rr.Code, http.StatusBadRequest)
	}

	// 3) testing case when tournament is absent
	actor = &dbActorMock{}
	actor.mbeginTransaction = func() (dbTransActor, error) {
		return actor, nil
	}
	actor.mgetTournament = func(tourId string) (uint, string, error) {
		return 0, "", sql.ErrNoRows
	}
	actor.mrollback = func() error {
		return nil
	}
	request3 := request1
	rr = serveHTTP("GET", request3, nil, actor, joinTournamentHandler, t)
	if rr.Code != http.StatusBadRequest || actor.isAllExpectedCall() == false {
		t.Errorf("handler returned wrong status code: got %v want %v",
			rr.Code, http.StatusBadRequest)
	}

	// 4) testing case when get tournament return db error
	actor = &dbActorMock{}
	actor.mbeginTransaction = func() (dbTransActor, error) {
		return actor, nil
	}
	actor.mgetTournament = func(tourId string) (uint, string, error) {
		return 0, "", fmt.Errorf("some db error")
	}
	actor.mrollback = func() error {
		return nil
	}
	request4 := request3
	rr = serveHTTP("GET", request4, nil, actor, joinTournamentHandler, t)
	if rr.Code != http.StatusInternalServerError || actor.isAllExpectedCall() == false {
		t.Errorf("handler returned wrong status code: got %v want %v",
			rr.Code, http.StatusInternalServerError)
	}

	// 5) testing case when player does not have much funds
	actor = &dbActorMock{}
	actor.mbeginTransaction = func() (dbTransActor, error) {
		return actor, nil
	}
	actor.mgetTournament = func(tourId string) (uint, string, error) {
		if tourId == tournamentID {
			return tournamentDeposit, "s", nil
		}
		return 0, "", fmt.Errorf("request 5 mock error 0")
	}
	actor.misPlayerExist = func(playerId string) (bool, error) {
		if playerID == playerId {
			return true, nil
		} else if baker1ID == playerId {
			return true, nil
		} else if baker2ID == playerId {
			return true, nil
		} else if baker3ID == playerId {
			return true, nil
		} else if baker4ID == playerId {
			return true, nil
		}
		return false, fmt.Errorf("request 5 mock error 1")
	}
	actor.mgetPlayerBalance = func(playerId string) (uint, error) {
		if playerID == playerId {
			return 100, nil
		} else if baker1ID == playerId {
			return baker1Balance, nil
		} else if baker2ID == playerId {
			return baker2Balance, nil
		} else if baker3ID == playerId {
			return baker3Balance, nil
		} else if baker4ID == playerId {
			return baker4Balance, nil
		}
		return 0, fmt.Errorf("request 5 mock error 2")
	}
	actor.mrollback = func() error {
		return nil
	}
	rr = serveHTTP("GET", request1, nil, actor, joinTournamentHandler, t)
	if rr.Code != http.StatusBadRequest || actor.isAllExpectedCall() == false {
		t.Errorf("handler returned wrong status code: got %v want %v",
			rr.Code, http.StatusBadRequest)
	}
	// 6) testing case when baker does not have much funds
	actor = &dbActorMock{}
	actor.mbeginTransaction = func() (dbTransActor, error) {
		return actor, nil
	}
	actor.mgetTournament = func(tourId string) (uint, string, error) {
		if tourId == tournamentID {
			return tournamentDeposit, "s", nil
		}
		return 0, "", fmt.Errorf("request 6 mock error 0")
	}
	actor.misPlayerExist = func(playerId string) (bool, error) {
		if playerID == playerId {
			return true, nil
		} else if baker1ID == playerId {
			return true, nil
		} else if baker2ID == playerId {
			return true, nil
		} else if baker3ID == playerId {
			return true, nil
		} else if baker4ID == playerId {
			return true, nil
		}
		return false, fmt.Errorf("request 6 mock error 1")
	}
	actor.mgetPlayerBalance = func(playerId string) (uint, error) {
		if playerID == playerId {
			return player1Balance, nil
		} else if baker1ID == playerId {
			return 100, nil
		} else if baker2ID == playerId {
			return baker2Balance, nil
		} else if baker3ID == playerId {
			return baker3Balance, nil
		} else if baker4ID == playerId {
			return baker4Balance, nil
		}
		return 0, fmt.Errorf("request 6 mock error 2")
	}
	actor.mtakePlayer = func(playerId string, amount uint) error {
		if playerID == playerId {
			return nil
		} else if baker1ID == playerId {
			return nil
		} else if baker2ID == playerId {
			return nil
		} else if baker3ID == playerId {
			return nil
		} else if baker4ID == playerId {
			return nil
		}
		return fmt.Errorf("request 6 mock error 3")
	}
	actor.mjoinTournament = func(tournamentId string, playerId string, joinerType string, bakerId string, Stake uint) error {
		if tournamentId == tournamentID && stake == Stake {
			if playerID == playerId {
				if joinerType == "b" {
					if baker1ID == bakerId {
						return nil
					} else if baker2ID == bakerId {
						return nil
					} else if baker3ID == bakerId {
						return nil
					} else if baker4ID == bakerId {
						return nil
					}
				} else if joinerType == "p" {
					return nil
				}
			}
		}
		return fmt.Errorf("request 6 mock error 4")
	}
	actor.mrollback = func() error {
		return nil
	}
	rr = serveHTTP("GET", request1, nil, actor, joinTournamentHandler, t)
	if rr.Code != http.StatusBadRequest || actor.isAllExpectedCall() == false {
		t.Errorf("handler returned wrong status code: got %v want %v",
			rr.Code, http.StatusBadRequest)
	}

	// 7) request tests case when getPlayerBalance return db error
	actor = &dbActorMock{}
	actor.mbeginTransaction = func() (dbTransActor, error) {
		return actor, nil
	}
	actor.mgetTournament = func(tourId string) (uint, string, error) {
		if tourId == tournamentID {
			return tournamentDeposit, "s", nil
		}
		return 0, "", fmt.Errorf("request 6 mock error 0")
	}
	actor.misPlayerExist = func(playerId string) (bool, error) {
		if playerID == playerId {
			return true, nil
		} else if baker1ID == playerId {
			return true, nil
		} else if baker2ID == playerId {
			return true, nil
		} else if baker3ID == playerId {
			return true, nil
		} else if baker4ID == playerId {
			return true, nil
		}
		return false, fmt.Errorf("request 6 mock error 1")
	}
	actor.mgetPlayerBalance = func(playerId string) (uint, error) {
		return 0, fmt.Errorf("db error")
	}
	actor.mrollback = func() error {
		return nil
	}
	rr = serveHTTP("GET", request1, nil, actor, joinTournamentHandler, t)
	if rr.Code != http.StatusInternalServerError || actor.isAllExpectedCall() == false {
		t.Errorf("handler returned wrong status code: got %v want %v",
			rr.Code, http.StatusInternalServerError)
	}

	// 8) request tests case when takePlayer return db error
	actor = &dbActorMock{}
	actor.mbeginTransaction = func() (dbTransActor, error) {
		return actor, nil
	}
	actor.mgetTournament = func(tourId string) (uint, string, error) {
		if tourId == tournamentID {
			return tournamentDeposit, "s", nil
		}
		return 0, "", fmt.Errorf("request 6 mock error 0")
	}
	actor.misPlayerExist = func(playerId string) (bool, error) {
		if playerID == playerId {
			return true, nil
		} else if baker1ID == playerId {
			return true, nil
		} else if baker2ID == playerId {
			return true, nil
		} else if baker3ID == playerId {
			return true, nil
		} else if baker4ID == playerId {
			return true, nil
		}
		return false, fmt.Errorf("request 6 mock error 1")
	}
	actor.mgetPlayerBalance = func(playerId string) (uint, error) {
		if playerID == playerId {
			return player1Balance, nil
		} else if baker1ID == playerId {
			return baker1Balance, nil
		} else if baker2ID == playerId {
			return baker2Balance, nil
		} else if baker3ID == playerId {
			return baker3Balance, nil
		} else if baker4ID == playerId {
			return baker4Balance, nil
		}
		return 0, fmt.Errorf("request 6 mock error 2")
	}
	actor.mtakePlayer = func(playerId string, amount uint) error {
		return fmt.Errorf("some db error")
	}
	actor.mrollback = func() error {
		return nil
	}
	rr = serveHTTP("GET", request1, nil, actor, joinTournamentHandler, t)
	if rr.Code != http.StatusInternalServerError || actor.isAllExpectedCall() == false {
		t.Errorf("handler returned wrong status code: got %v want %v",
			rr.Code, http.StatusInternalServerError)
	}

	// 9) request tests case when joinTournament return db error
	actor = &dbActorMock{}
	actor.mbeginTransaction = func() (dbTransActor, error) {
		return actor, nil
	}
	actor.mgetTournament = func(tourId string) (uint, string, error) {
		if tourId == tournamentID {
			return tournamentDeposit, "s", nil
		}
		return 0, "", fmt.Errorf("request 9 mock error 0")
	}
	actor.misPlayerExist = func(playerId string) (bool, error) {
		if playerID == playerId {
			return true, nil
		} else if baker1ID == playerId {
			return true, nil
		} else if baker2ID == playerId {
			return true, nil
		} else if baker3ID == playerId {
			return true, nil
		} else if baker4ID == playerId {
			return true, nil
		}
		return false, fmt.Errorf("request 9 mock error 1")
	}
	actor.mgetPlayerBalance = func(playerId string) (uint, error) {
		if playerID == playerId {
			return player1Balance, nil
		} else if baker1ID == playerId {
			return baker1Balance, nil
		} else if baker2ID == playerId {
			return baker2Balance, nil
		} else if baker3ID == playerId {
			return baker3Balance, nil
		} else if baker4ID == playerId {
			return baker4Balance, nil
		}
		return 0, fmt.Errorf("request 9 mock error 2")
	}
	actor.mtakePlayer = func(playerId string, amount uint) error {
		if playerID == playerId {
			return nil
		} else if baker1ID == playerId {
			return nil
		} else if baker2ID == playerId {
			return nil
		} else if baker3ID == playerId {
			return nil
		} else if baker4ID == playerId {
			return nil
		}
		return fmt.Errorf("request 9 mock error 3")
	}

	actor.mjoinTournament = func(tournamentId string, playerId string, joinerType string, bakerId string, Stake uint) error {
		return fmt.Errorf("some db error")
	}

	actor.mrollback = func() error {
		return nil
	}
	rr = serveHTTP("GET", request1, nil, actor, joinTournamentHandler, t)
	if rr.Code != http.StatusInternalServerError || actor.isAllExpectedCall() == false {
		t.Errorf("handler returned wrong status code: got %v want %v",
			rr.Code, http.StatusInternalServerError)
	}

	// 10) request tests case when joinTournament baker1 return db error
	actor = &dbActorMock{}
	actor.mbeginTransaction = func() (dbTransActor, error) {
		return actor, nil
	}
	actor.mgetTournament = func(tourId string) (uint, string, error) {
		if tourId == tournamentID {
			return tournamentDeposit, "s", nil
		}
		return 0, "", fmt.Errorf("request 10 mock error 0")
	}
	actor.misPlayerExist = func(playerId string) (bool, error) {
		if playerID == playerId {
			return true, nil
		} else if baker1ID == playerId {
			return true, nil
		} else if baker2ID == playerId {
			return true, nil
		} else if baker3ID == playerId {
			return true, nil
		} else if baker4ID == playerId {
			return true, nil
		}
		return false, fmt.Errorf("request 10 mock error 1")
	}
	actor.mgetPlayerBalance = func(playerId string) (uint, error) {
		if playerID == playerId {
			return player1Balance, nil
		} else if baker1ID == playerId {
			return baker1Balance, nil
		} else if baker2ID == playerId {
			return baker2Balance, nil
		} else if baker3ID == playerId {
			return baker3Balance, nil
		} else if baker4ID == playerId {
			return baker4Balance, nil
		}
		return 0, fmt.Errorf("request 10 mock error 2")
	}
	actor.mtakePlayer = func(playerId string, amount uint) error {
		if playerID == playerId {
			return nil
		} else if baker1ID == playerId {
			return nil
		} else if baker2ID == playerId {
			return nil
		} else if baker3ID == playerId {
			return nil
		} else if baker4ID == playerId {
			return nil
		}
		return fmt.Errorf("request 10 mock error 3")
	}

	actor.mjoinTournament = func(tournamentId string, playerId string, joinerType string, bakerId string, Stake uint) error {
		if tournamentId == tournamentID && stake == Stake {
			if playerID == playerId {
				if joinerType == "b" {
					return fmt.Errorf("some db error")
				} else if joinerType == "p" {
					return nil
				}
			}
		}
		return fmt.Errorf("request 10 mock error 4")
	}

	actor.mrollback = func() error {
		return nil
	}
	rr = serveHTTP("GET", request1, nil, actor, joinTournamentHandler, t)
	if rr.Code != http.StatusInternalServerError || actor.isAllExpectedCall() == false {
		t.Errorf("handler returned wrong status code: got %v want %v",
			rr.Code, http.StatusInternalServerError)
	}

	// 11) request tests case when joinTournament baker1 return db error
	actor = &dbActorMock{}
	actor.mbeginTransaction = func() (dbTransActor, error) {
		return actor, nil
	}
	actor.mgetTournament = func(tourId string) (uint, string, error) {
		if tourId == tournamentID {
			return tournamentDeposit, "s", nil
		}
		return 0, "", fmt.Errorf("request 10 mock error 0")
	}
	actor.misPlayerExist = func(playerId string) (bool, error) {
		if playerID == playerId {
			return true, nil
		} else if baker1ID == playerId {
			return true, nil
		} else if baker2ID == playerId {
			return true, nil
		} else if baker3ID == playerId {
			return true, nil
		} else if baker4ID == playerId {
			return true, nil
		}
		return false, fmt.Errorf("request 10 mock error 1")
	}
	actor.mgetPlayerBalance = func(playerId string) (uint, error) {
		if playerID == playerId {
			return player1Balance, nil
		} else if baker1ID == playerId {
			return baker1Balance, nil
		} else if baker2ID == playerId {
			return baker2Balance, nil
		} else if baker3ID == playerId {
			return baker3Balance, nil
		} else if baker4ID == playerId {
			return baker4Balance, nil
		}
		return 0, fmt.Errorf("request 10 mock error 2")
	}
	actor.mtakePlayer = func(playerId string, amount uint) error {
		if playerID == playerId {
			return nil
		}
		return fmt.Errorf("some db error")
	}

	actor.mjoinTournament = func(tournamentId string, playerId string, joinerType string, bakerId string, Stake uint) error {
		return nil
	}

	actor.mrollback = func() error {
		return nil
	}
	rr = serveHTTP("GET", request1, nil, actor, joinTournamentHandler, t)
	if rr.Code != http.StatusInternalServerError || actor.isAllExpectedCall() == false {
		t.Errorf("handler returned wrong status code: got %v want %v",
			rr.Code, http.StatusInternalServerError)
	}

	// 12) request tests case when joinTournament baker1 return db error
	actor = &dbActorMock{}
	actor.mbeginTransaction = func() (dbTransActor, error) {
		return actor, nil
	}
	actor.mgetTournament = func(tourId string) (uint, string, error) {
		if tourId == tournamentID {
			return tournamentDeposit, "s", nil
		}
		return 0, "", fmt.Errorf("request 12 mock error 0")
	}
	actor.misPlayerExist = func(playerId string) (bool, error) {
		if playerID == playerId {
			return true, nil
		} else if baker1ID == playerId {
			return true, nil
		} else if baker2ID == playerId {
			return true, nil
		} else if baker3ID == playerId {
			return true, nil
		} else if baker4ID == playerId {
			return true, nil
		}
		return false, fmt.Errorf("request 12 mock error 1")
	}
	actor.mgetPlayerBalance = func(playerId string) (uint, error) {
		if playerID == playerId {
			return player1Balance, nil
		} else if baker1ID == playerId {
			return baker1Balance, nil
		} else if baker2ID == playerId {
			return baker2Balance, nil
		} else if baker3ID == playerId {
			return baker3Balance, nil
		} else if baker4ID == playerId {
			return baker4Balance, nil
		}
		return 0, fmt.Errorf("request 12 mock error 2")
	}
	actor.mtakePlayer = func(playerId string, amount uint) error {
		if playerID == playerId {
			return nil
		} else if baker1ID == playerId {
			return nil
		} else if baker2ID == playerId {
			return nil
		} else if baker3ID == playerId {
			return nil
		} else if baker4ID == playerId {
			return nil
		}
		return fmt.Errorf("request 1 mock error 3")
	}

	actor.mjoinTournament = func(tournamentId string, playerId string, joinerType string, bakerId string, Stake uint) error {
		if tournamentId == tournamentID && stake == Stake {
			if playerID == playerId {
				if joinerType == "b" {
					return fmt.Errorf("some db error")
				} else if joinerType == "p" {
					return nil
				}
			}
		}
		return fmt.Errorf("request 13 mock error 3")
	}
	actor.mrollback = func() error {
		return nil
	}

	rr = serveHTTP("GET", request1, nil, actor, joinTournamentHandler, t)
	if rr.Code != http.StatusInternalServerError || actor.isAllExpectedCall() == false {
		t.Errorf("handler returned wrong status code: got %v want %v",
			rr.Code, http.StatusInternalServerError)
	}
	// 13) case when begin transaction returned error
	actor = &dbActorMock{}
	actor.mbeginTransaction = func() (dbTransActor, error) {
		return nil, fmt.Errorf("some db error")
	}
	rr = serveHTTP("GET", request1, nil, actor, joinTournamentHandler, t)
	if rr.Code != http.StatusInternalServerError || actor.isAllExpectedCall() == false {
		t.Errorf("handler returned wrong status code: got %v want %v",
			rr.Code, http.StatusInternalServerError)
	}

	// 14) case when commit failed
	actor = &dbActorMock{}
	actor.mbeginTransaction = func() (dbTransActor, error) {
		return actor, nil
	}
	actor.mgetTournament = func(tourId string) (uint, string, error) {
		if tourId == tournamentID {
			return tournamentDeposit, "s", nil
		}
		return 0, "", fmt.Errorf("request 14 mock error 0")
	}
	actor.misPlayerExist = func(playerId string) (bool, error) {
		if playerID == playerId {
			return true, nil
		}
		return false, fmt.Errorf("request 14 mock error 1")
	}
	actor.mgetPlayerBalance = func(playerId string) (uint, error) {
		if playerID == playerId {
			return player1Balance, nil
		}
		return 0, fmt.Errorf("request 14 mock error 2")
	}
	actor.mtakePlayer = func(playerId string, amount uint) error {
		if playerID == playerId {
			return nil
		}
		return fmt.Errorf("request 14 mock error 3")
	}
	actor.mjoinTournament = func(tournamentId string, playerId string, joinerType string, bakerId string, Stake uint) error {
		if tournamentId == tournamentID && tournamentDeposit == Stake {
			if playerID == playerId {
				return nil
			}
		}
		return fmt.Errorf("request 14 mock error 3")
	}
	actor.mcommit = func() error {
		return fmt.Errorf("some db error")
	}
	actor.mrollback = func() error {
		return nil
	}
	request14 := fmt.Sprintf("http://localhost:8080/joinTournament?tournamentId=%s&playerId=%s", tournamentID, playerID)
	rr = serveHTTP("GET", request14, nil, actor, joinTournamentHandler, t)
	if rr.Code != http.StatusInternalServerError || actor.isAllExpectedCall() == false {
		t.Errorf("handler returned wrong status code: got %v want %v",
			rr.Code, http.StatusInternalServerError)
	}

	// 15) case when commit failed
	actor = &dbActorMock{}
	actor.mbeginTransaction = func() (dbTransActor, error) {
		return actor, nil
	}
	actor.mgetTournament = func(tourId string) (uint, string, error) {
		return 0, "", fmt.Errorf("some db error")
	}
	actor.mrollback = func() error {
		return nil
	}
	request15 := fmt.Sprintf("http://localhost:8080/joinTournament?tournamentId=%s&playerId=%s", tournamentID, playerID)
	rr = serveHTTP("GET", request15, nil, actor, joinTournamentHandler, t)
	if rr.Code != http.StatusInternalServerError || actor.isAllExpectedCall() == false {
		t.Errorf("handler returned wrong status code: got %v want %v",
			rr.Code, http.StatusInternalServerError)
	}
	//16) case when isPlayerExist return db error
	actor = &dbActorMock{}
	actor.mbeginTransaction = func() (dbTransActor, error) {
		return actor, nil
	}
	actor.mgetTournament = func(tourId string) (uint, string, error) {
		if tourId == tournamentID {
			return tournamentDeposit, "s", nil
		}
		return 0, "", fmt.Errorf("request 15 mock error 0")
	}
	actor.misPlayerExist = func(playerId string) (bool, error) {
		return false, fmt.Errorf("return some db")
	}
	actor.mrollback = func() error {
		return nil
	}
	request16 := fmt.Sprintf("http://localhost:8080/joinTournament?tournamentId=%s&playerId=%s", tournamentID, playerID)
	rr = serveHTTP("GET", request16, nil, actor, joinTournamentHandler, t)
	if rr.Code != http.StatusInternalServerError || actor.isAllExpectedCall() == false {
		t.Errorf("handler returned wrong status code: got %v want %v",
			rr.Code, http.StatusInternalServerError)
	}
	//17) case when getPlayerBalance return no rows
	actor = &dbActorMock{}
	actor.mbeginTransaction = func() (dbTransActor, error) {
		return actor, nil
	}
	actor.mgetTournament = func(tourId string) (uint, string, error) {
		if tourId == tournamentID {
			return tournamentDeposit, "s", nil
		}
		return 0, "", fmt.Errorf("request 17 mock error 0")
	}
	actor.misPlayerExist = func(playerId string) (bool, error) {
		return true, nil
	}
	actor.mgetPlayerBalance = func(playerId string) (uint, error) {
		return 0, sql.ErrNoRows
	}

	actor.mrollback = func() error {
		return nil
	}
	rr = serveHTTP("GET", request1, nil, actor, joinTournamentHandler, t)
	if rr.Code != http.StatusBadRequest || actor.isAllExpectedCall() == false {
		t.Errorf("handler returned wrong status code: got %v want %v",
			rr.Code, http.StatusBadRequest)
	}
	//18) case when getPlayerBalance return db error
	actor = &dbActorMock{}
	actor.mbeginTransaction = func() (dbTransActor, error) {
		return actor, nil
	}
	actor.mgetTournament = func(tourId string) (uint, string, error) {
		if tourId == tournamentID {
			return tournamentDeposit, "s", nil
		}
		return 0, "", fmt.Errorf("request 17 mock error 0")
	}
	actor.misPlayerExist = func(playerId string) (bool, error) {
		return true, nil
	}
	actor.mgetPlayerBalance = func(playerId string) (uint, error) {
		return 0, fmt.Errorf("db error")
	}

	actor.mrollback = func() error {
		return nil
	}
	rr = serveHTTP("GET", request1, nil, actor, joinTournamentHandler, t)
	if rr.Code != http.StatusInternalServerError || actor.isAllExpectedCall() == false {
		t.Errorf("handler returned wrong status code: got %v want %v",
			rr.Code, http.StatusInternalServerError)
	}
	// 18) testing case when player with playerID is absent
	request18 := fmt.Sprintf("http://localhost:8080/joinTournament?tournamentId=%s", tournamentID)
	rr = serveHTTP("GET", request18, nil, nil, joinTournamentHandler, t)
	if rr.Code != http.StatusBadRequest {
		t.Errorf("handler returned wrong status code: got %v want %v",
			rr.Code, http.StatusBadRequest)
	}

	// 19) testing case when player with tournamentID is absent
	request19 := fmt.Sprintf("http://localhost:8080/joinTournament?&playerId=%s", playerID)
	rr = serveHTTP("GET", request19, nil, nil, joinTournamentHandler, t)
	if rr.Code != http.StatusBadRequest {
		t.Errorf("handler returned wrong status code: got %v want %v",
			rr.Code, http.StatusBadRequest)
	}

	// 20) request tests case when baker1Id is absent
	actor = &dbActorMock{}
	actor.mbeginTransaction = func() (dbTransActor, error) {
		return actor, nil
	}
	actor.mgetTournament = func(tourId string) (uint, string, error) {
		if tourId == tournamentID {
			return tournamentDeposit, "s", nil
		}
		return 0, "", fmt.Errorf("request 12 mock error 0")
	}
	actor.misPlayerExist = func(playerId string) (bool, error) {
		if playerID == playerId {
			return true, nil
		} else if baker1ID == playerId {
			return false, sql.ErrNoRows
		} else if baker2ID == playerId {
			return true, nil
		} else if baker3ID == playerId {
			return true, nil
		} else if baker4ID == playerId {
			return true, nil
		}
		return false, fmt.Errorf("request 12 mock error 1")
	}
	actor.mgetPlayerBalance = func(playerId string) (uint, error) {
		if playerID == playerId {
			return player1Balance, nil
		} else if baker1ID == playerId {
			return baker1Balance, nil
		} else if baker2ID == playerId {
			return baker2Balance, nil
		} else if baker3ID == playerId {
			return baker3Balance, nil
		} else if baker4ID == playerId {
			return baker4Balance, nil
		}
		return 0, fmt.Errorf("request 12 mock error 2")
	}
	actor.mtakePlayer = func(playerId string, amount uint) error {
		if playerID == playerId {
			return nil
		} else if baker1ID == playerId {
			return nil
		} else if baker2ID == playerId {
			return nil
		} else if baker3ID == playerId {
			return nil
		} else if baker4ID == playerId {
			return nil
		}
		return fmt.Errorf("request 1 mock error 3")
	}

	actor.mjoinTournament = func(tournamentId string, playerId string, joinerType string, bakerId string, Stake uint) error {
		if tournamentId == tournamentID && stake == Stake {
			if playerID == playerId {
				if joinerType == "b" {
					return fmt.Errorf("some db error")
				} else if joinerType == "p" {
					return nil
				}
			}
		}
		return fmt.Errorf("request 13 mock error 3")
	}
	actor.mrollback = func() error {
		return nil
	}
	// 20) testing case when baker with baker1ID is absent
	rr = serveHTTP("GET", request1, nil, actor, joinTournamentHandler, t)
	if rr.Code != http.StatusBadRequest {
		t.Errorf("handler returned wrong status code: got %v want %v",
			rr.Code, http.StatusBadRequest)
	}

	// 21) testing case when checking baker1 presents have an error by db
	actor = &dbActorMock{}
	actor.mbeginTransaction = func() (dbTransActor, error) {
		return actor, nil
	}
	actor.mgetTournament = func(tourId string) (uint, string, error) {
		if tourId == tournamentID {
			return tournamentDeposit, "s", nil
		}
		return 0, "", fmt.Errorf("request 12 mock error 0")
	}
	actor.misPlayerExist = func(playerId string) (bool, error) {
		if playerID == playerId {
			return true, nil
		} else if baker1ID == playerId {
			return false, fmt.Errorf("db error")
		} else if baker2ID == playerId {
			return true, nil
		} else if baker3ID == playerId {
			return true, nil
		} else if baker4ID == playerId {
			return true, nil
		}
		return false, fmt.Errorf("request 12 mock error 1")
	}
	actor.mgetPlayerBalance = func(playerId string) (uint, error) {
		if playerID == playerId {
			return player1Balance, nil
		} else if baker1ID == playerId {
			return baker1Balance, nil
		} else if baker2ID == playerId {
			return baker2Balance, nil
		} else if baker3ID == playerId {
			return baker3Balance, nil
		} else if baker4ID == playerId {
			return baker4Balance, nil
		}
		return 0, fmt.Errorf("request 12 mock error 2")
	}
	actor.mtakePlayer = func(playerId string, amount uint) error {
		if playerID == playerId {
			return nil
		} else if baker1ID == playerId {
			return nil
		} else if baker2ID == playerId {
			return nil
		} else if baker3ID == playerId {
			return nil
		} else if baker4ID == playerId {
			return nil
		}
		return fmt.Errorf("request 1 mock error 3")
	}

	actor.mjoinTournament = func(tournamentId string, playerId string, joinerType string, bakerId string, Stake uint) error {
		if tournamentId == tournamentID && stake == Stake {
			if playerID == playerId {
				if joinerType == "b" {
					return fmt.Errorf("some db error")
				} else if joinerType == "p" {
					return nil
				}
			}
		}
		return fmt.Errorf("request 13 mock error 3")
	}
	actor.mrollback = func() error {
		return nil
	}
	rr = serveHTTP("GET", request1, nil, actor, joinTournamentHandler, t)
	if rr.Code != http.StatusInternalServerError {
		t.Errorf("handler returned wrong status code: got %v want %v",
			rr.Code, http.StatusInternalServerError)
	}

	// 22) testing case when checking baker1 balance have an error by db
	actor = &dbActorMock{}
	actor.mbeginTransaction = func() (dbTransActor, error) {
		return actor, nil
	}
	actor.mgetTournament = func(tourId string) (uint, string, error) {
		if tourId == tournamentID {
			return tournamentDeposit, "s", nil
		}
		return 0, "", fmt.Errorf("request 12 mock error 0")
	}
	actor.misPlayerExist = func(playerId string) (bool, error) {
		if playerID == playerId {
			return true, nil
		} else if baker1ID == playerId {
			return true, nil
		} else if baker2ID == playerId {
			return true, nil
		} else if baker3ID == playerId {
			return true, nil
		} else if baker4ID == playerId {
			return true, nil
		}
		return false, fmt.Errorf("request 12 mock error 1")
	}
	actor.mgetPlayerBalance = func(playerId string) (uint, error) {
		if playerID == playerId {
			return player1Balance, nil
		} else if baker1ID == playerId {
			return 0, fmt.Errorf("db error")
		} else if baker2ID == playerId {
			return baker2Balance, nil
		} else if baker3ID == playerId {
			return baker3Balance, nil
		} else if baker4ID == playerId {
			return baker4Balance, nil
		}
		return 0, fmt.Errorf("request 12 mock error 2")
	}
	actor.mtakePlayer = func(playerId string, amount uint) error {
		if playerID == playerId {
			return nil
		} else if baker1ID == playerId {
			return nil
		} else if baker2ID == playerId {
			return nil
		} else if baker3ID == playerId {
			return nil
		} else if baker4ID == playerId {
			return nil
		}
		return fmt.Errorf("request 1 mock error 3")
	}

	actor.mjoinTournament = func(tournamentId string, playerId string, joinerType string, bakerId string, Stake uint) error {
		if tournamentId == tournamentID && stake == Stake {
			if playerID == playerId {
				if joinerType == "b" {
					return fmt.Errorf("some db error")
				} else if joinerType == "p" {
					return nil
				}
			}
		}
		return fmt.Errorf("request 13 mock error 3")
	}
	actor.mrollback = func() error {
		return nil
	}
	rr = serveHTTP("GET", request1, nil, actor, joinTournamentHandler, t)
	if rr.Code != http.StatusInternalServerError {
		t.Errorf("handler returned wrong status code: got %v want %v",
			rr.Code, http.StatusInternalServerError)
	}

	//23) case when isPlayerExist return no rows
	actor = &dbActorMock{}
	actor.mbeginTransaction = func() (dbTransActor, error) {
		return actor, nil
	}
	actor.mgetTournament = func(tourId string) (uint, string, error) {
		if tourId == tournamentID {
			return tournamentDeposit, "s", nil
		}
		return 0, "", fmt.Errorf("request 23 mock error 0")
	}
	actor.misPlayerExist = func(playerId string) (bool, error) {
		return false, sql.ErrNoRows
	}
	actor.mrollback = func() error {
		return nil
	}
	rr = serveHTTP("GET", request1, nil, actor, joinTournamentHandler, t)
	if rr.Code != http.StatusBadRequest || actor.isAllExpectedCall() == false {
		t.Errorf("handler returned wrong status code: got %v want %v",
			rr.Code, http.StatusBadRequest)
	}

	// 1 request
	/*


		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expections: %s", err)
		}*/
}

func resultJSON(result *resultTournament) *bytes.Buffer {
	bResult, _ := json.Marshal(result)
	//fmt.Printf(string(bResult))
	return bytes.NewBuffer(bResult)
}

func resultStringToBuffer(str string) *bytes.Buffer {
	return bytes.NewBuffer([]byte(str))
}

func TestResultTournamentHandler(t *testing.T) {
	// 1 request success case
	//tournamentId=1&playerId=P1&backerId=P2&backerId=P3&backerId=P4
	tournamentID := "1"
	tournamentDeposit := uint(1000)
	totalStake := uint(2000)
	player1ID := "P1"
	backer1ID := "P2"
	backer2ID := "P3"
	backer3ID := "P4"
	playerAndBakerCount := 4
	stake := uint(float32(tournamentDeposit) / float32(playerAndBakerCount))
	player1TotalStake := stake * uint(playerAndBakerCount)
	player2TotalStake := tournamentDeposit
	player2ID := "P5"
	winnerID := player1ID
	winnerPrize := totalStake
	eachWinnerPrize := uint(float32(totalStake) / float32(playerAndBakerCount))

	var result1 resultTournament
	result1.TournamentId = tournamentID
	result1.addWinner(winnerID, winnerPrize)

	actor := &dbActorMock{}
	actor.mbeginTransaction = func() (dbTransActor, error) {
		return actor, nil
	}
	actor.mgetTournament = func(tourId string) (uint, string, error) {
		if tourId == tournamentID {
			return tournamentDeposit, "s", nil
		}
		return 0, "", fmt.Errorf("request 1 mock error 0")
	}

	actor.mgetTournamentTotalStake = func(tourmID string) (uint, error) {
		if tourmID == tournamentID {
			return totalStake, nil
		}
		return 0, sql.ErrNoRows
	}
	actor.mgetTournamentTotalPlayerStake = func(tourmID string, playID string) (uint, error) {
		if tourmID == tournamentID {
			if playID == player1ID {
				return tournamentDeposit, nil
			} else if playID == player2ID {
				return tournamentDeposit, nil
			}
			return 0, sql.ErrNoRows
		}
		return 0, sql.ErrNoRows
	}
	actor.mgetTournamentPlayerBakers = func(tournID string, playID string, inject func(linkid uint, playID string, bakerType string, bakerId string, stake uint) bool) error {
		if tournID == tournamentID {
			if playID == player1ID {
				for i := 1; i < 4; i++ {
					switch {
					case i == 1:
						inject(10, player1ID, "p", "", stake)
					case i == 2:
						inject(10, player1ID, "b", backer1ID, stake)
					case i == 3:
						inject(10, player1ID, "b", backer2ID, stake)
					}
				}
				return nil
			} else if playID == player2ID {
				inject(10, player2ID, "p", "", tournamentDeposit)
				return nil
			}
		}
		return sql.ErrNoRows
	}
	actor.mfundPlayer = func(playerId string, amount uint) error {
		if amount != eachWinnerPrize {
			return fmt.Errorf("error - amount!=eachWinnerPrize")
		}
		if player1ID == playerId {
			return nil
		} else if backer1ID == playerId {
			return nil
		} else if backer2ID == playerId {
			return nil
		} else if backer3ID == playerId {
			return nil
		} else if player2ID == playerId {
			return fmt.Errorf("error - this player does not have prize")
		}
		return fmt.Errorf("no such player %s", playerId)
	}
	actor.mcloseTournament = func(tourmID string) error {
		return nil
	}
	actor.mcommit = func() error {
		return nil
	}
	successCaseActor := *actor
	rr := serveHTTP("POST", "http://localhost:8080/resultTournament", resultJSON(&result1), actor, resultTournamentHandler, t)
	if rr.Code != http.StatusOK || actor.isAllExpectedCall() == false {
		t.Errorf("handler returned wrong status code: got %v want %v",
			rr.Code, http.StatusOK)
	}

	// 2 breaked JSON request
	rr = serveHTTP("POST", "http://localhost:8080/resultTournament", resultStringToBuffer("{\"tournamentId\": \"1\",\"winners\": [{\"playerId\": \"P1\", \"prize\": 2000}}"), actor, resultTournamentHandler, t)
	if rr.Code != http.StatusBadRequest || actor.isAllExpectedCall() == false {
		t.Errorf("handler returned wrong status code: got %v want %v",
			rr.Code, http.StatusBadRequest)
	}
	// 3 start transaction failed
	actor = &dbActorMock{}
	actor.mbeginTransaction = func() (dbTransActor, error) {
		return actor, fmt.Errorf("db error")
	}
	rr = serveHTTP("POST", "http://localhost:8080/resultTournament", resultJSON(&result1), actor, resultTournamentHandler, t)
	if rr.Code != http.StatusInternalServerError || actor.isAllExpectedCall() == false {
		t.Errorf("handler returned wrong status code: got %v want %v",
			rr.Code, http.StatusInternalServerError)
	}
	// 4 request commit failed
	copy := successCaseActor
	actor = &copy
	actor.reset()
	actor.mcommit = func() error {
		return fmt.Errorf("db error")
	}
	actor.mrollback = func() error {
		return nil
	}
	rr = serveHTTP("POST", "http://localhost:8080/resultTournament", resultJSON(&result1), actor, resultTournamentHandler, t)
	if rr.Code != http.StatusInternalServerError || actor.isAllExpectedCall() == false {
		t.Errorf("handler returned wrong status code: got %v want %v",
			rr.Code, http.StatusInternalServerError)
	}
	// 5 request get tournament return NoSqlRows
	actor = &dbActorMock{}
	actor.mgetTournament = func(tourId string) (uint, string, error) {
		return 0, "", sql.ErrNoRows
	}
	actor.mbeginTransaction = func() (dbTransActor, error) {
		return actor, nil
	}
	actor.mrollback = func() error {
		return nil
	}
	rr = serveHTTP("POST", "http://localhost:8080/resultTournament", resultJSON(&result1), actor, resultTournamentHandler, t)
	if rr.Code != http.StatusBadRequest || actor.isAllExpectedCall() == false {
		t.Errorf("handler returned wrong status code: got %v want %v",
			rr.Code, http.StatusBadRequest)
	}
	// 6 request get tournament return db error
	actor = &dbActorMock{}
	actor.mgetTournament = func(tourId string) (uint, string, error) {
		return 0, "", fmt.Errorf("db error")
	}
	actor.mbeginTransaction = func() (dbTransActor, error) {
		return actor, nil
	}
	actor.mrollback = func() error {
		return nil
	}
	rr = serveHTTP("POST", "http://localhost:8080/resultTournament", resultJSON(&result1), actor, resultTournamentHandler, t)
	if rr.Code != http.StatusInternalServerError || actor.isAllExpectedCall() == false {
		t.Errorf("handler returned wrong status code: got %v want %v",
			rr.Code, http.StatusInternalServerError)
	}
	// 7 request get tournament return closed tournament
	actor = &dbActorMock{}
	actor.mgetTournament = func(tourId string) (uint, string, error) {
		return 1000, "c", nil
	}
	actor.mbeginTransaction = func() (dbTransActor, error) {
		return actor, nil
	}
	actor.mrollback = func() error {
		return nil
	}
	rr = serveHTTP("POST", "http://localhost:8080/resultTournament", resultJSON(&result1), actor, resultTournamentHandler, t)
	if rr.Code != http.StatusBadRequest || actor.isAllExpectedCall() == false {
		t.Errorf("handler returned wrong status code: got %v want %v",
			rr.Code, http.StatusBadRequest)
	}

	// 8 request get total stake tournament return error
	actor = &dbActorMock{}
	actor.mgetTournament = func(tourId string) (uint, string, error) {
		if tourId == tournamentID {
			return 1000, "s", nil
		}
		return 0, "", sql.ErrNoRows
	}
	actor.mbeginTransaction = func() (dbTransActor, error) {
		return actor, nil
	}
	actor.mgetTournamentTotalStake = func(tourmID string) (uint, error) {
		return 0, fmt.Errorf("db error")
	}
	actor.mrollback = func() error {
		return nil
	}
	rr = serveHTTP("POST", "http://localhost:8080/resultTournament", resultJSON(&result1), actor, resultTournamentHandler, t)
	if rr.Code != http.StatusInternalServerError || actor.isAllExpectedCall() == false {
		t.Errorf("handler returned wrong status code: got %v want %v",
			rr.Code, http.StatusInternalServerError)
	}

	// 9 request get total stake tournament return NoRows
	actor = &dbActorMock{}
	actor.mgetTournament = func(tourId string) (uint, string, error) {
		if tourId == tournamentID {
			return 1000, "s", nil
		}
		return 0, "", sql.ErrNoRows
	}
	actor.mbeginTransaction = func() (dbTransActor, error) {
		return actor, nil
	}
	actor.mgetTournamentTotalStake = func(tourmID string) (uint, error) {
		return 0, sql.ErrNoRows
	}
	actor.mrollback = func() error {
		return nil
	}
	rr = serveHTTP("POST", "http://localhost:8080/resultTournament", resultJSON(&result1), actor, resultTournamentHandler, t)
	if rr.Code != http.StatusBadRequest || actor.isAllExpectedCall() == false {
		t.Errorf("handler returned wrong status code: got %v want %v",
			rr.Code, http.StatusBadRequest)
	}

	// 10 request get total stake tournament return prize not equal requested prize
	actor = &dbActorMock{}
	actor.mgetTournament = func(tourId string) (uint, string, error) {
		if tourId == tournamentID {
			return 1000, "s", nil
		}
		return 0, "", sql.ErrNoRows
	}
	actor.mbeginTransaction = func() (dbTransActor, error) {
		return actor, nil
	}
	actor.mgetTournamentTotalStake = func(tourmID string) (uint, error) {
		return totalStake - 1, nil
	}
	actor.mrollback = func() error {
		return nil
	}
	rr = serveHTTP("POST", "http://localhost:8080/resultTournament", resultJSON(&result1), actor, resultTournamentHandler, t)
	if rr.Code != http.StatusBadRequest || actor.isAllExpectedCall() == false {
		t.Errorf("handler returned wrong status code: got %v want %v",
			rr.Code, http.StatusBadRequest)
	}

	// 11 request getTournamentTotalPlayerStake return sql.ErrNoRows
	successCase11 := &dbActorMock{}
	successCase11.mgetTournament = func(tourId string) (uint, string, error) {
		if tourId == tournamentID {
			return 1000, "s", nil
		}
		return 0, "", sql.ErrNoRows
	}
	successCase11.mbeginTransaction = func() (dbTransActor, error) {
		return actor, nil
	}
	successCase11.mgetTournamentTotalStake = func(tourmID string) (uint, error) {
		if tourmID == tournamentID {
			return totalStake, nil
		}
		return 0, sql.ErrNoRows
	}
	successCase11.mgetTournamentTotalPlayerStake = func(tourmID string, playID string) (uint, error) {
		if tourmID == tournamentID && playID == player1ID {
			return player1TotalStake, nil
		} else if tourmID == tournamentID && playID == player2ID {
			return player2TotalStake, nil
		}
		return 0, sql.ErrNoRows
	}
	copy = *successCase11
	actor = &copy
	actor.mgetTournamentTotalPlayerStake = func(tourmID string, playID string) (uint, error) {
		return 0, sql.ErrNoRows
	}
	actor.mrollback = func() error {
		return nil
	}
	rr = serveHTTP("POST", "http://localhost:8080/resultTournament", resultJSON(&result1), actor, resultTournamentHandler, t)
	if rr.Code != http.StatusBadRequest || actor.isAllExpectedCall() == false {
		t.Errorf("handler returned wrong status code: got %v want %v",
			rr.Code, http.StatusBadRequest)
	}

	// 12 request getTournamentTotalPlayerStake return db error
	copy = *successCase11
	actor = &copy
	actor.mgetTournamentTotalPlayerStake = func(tourmID string, playID string) (uint, error) {
		return 0, fmt.Errorf("db error")
	}
	actor.mrollback = func() error {
		return nil
	}
	rr = serveHTTP("POST", "http://localhost:8080/resultTournament", resultJSON(&result1), actor, resultTournamentHandler, t)
	if rr.Code != http.StatusInternalServerError || actor.isAllExpectedCall() == false {
		t.Errorf("handler returned wrong status code: got %v want %v",
			rr.Code, http.StatusInternalServerError)
	}

	// 13 request getTournamentPlayerBakers return db error
	copy = *successCase11
	successCase13 := copy
	successCase13.mgetTournamentPlayerBakers = func(tournID string, playID string, inject func(linkid uint, playerId string, bakerType string, bakerid string, stake uint) bool) (resErr error) {
		if tournID == tournamentID {
			if playID == player1ID {
				for i := 1; i < 4; i++ {
					switch {
					case i == 1:
						inject(10, player1ID, "p", "", stake)
					case i == 2:
						inject(10, player1ID, "b", backer1ID, stake)
					case i == 3:
						inject(10, player1ID, "b", backer2ID, stake)
					}
				}
				return nil
			} else if playID == player2ID {
				inject(10, player2ID, "p", "", tournamentDeposit)
				return nil
			}
		}
		return sql.ErrNoRows
	}
	copy = successCase13
	actor = &copy
	actor.mgetTournamentPlayerBakers = func(tournID string, playID string, inject func(linkid uint, playerId string, bakerType string, bakerid string, stake uint) bool) (resErr error) {
		return fmt.Errorf("db error")
	}
	actor.mrollback = func() error {
		return nil
	}
	rr = serveHTTP("POST", "http://localhost:8080/resultTournament", resultJSON(&result1), actor, resultTournamentHandler, t)
	if rr.Code != http.StatusInternalServerError || actor.isAllExpectedCall() == false {
		t.Errorf("handler returned wrong status code: got %v want %v",
			rr.Code, http.StatusInternalServerError)
	}
	// 14 request first call fundPlayer return db error
	copy = successCase13
	successCase14 := copy
	successCase14.mfundPlayer = func(playerId string, amount uint) error {
		if amount != eachWinnerPrize {
			return fmt.Errorf("error - amount!=eachWinnerPrize")
		}
		if player1ID == playerId {
			return nil
		} else if backer1ID == playerId {
			return nil
		} else if backer2ID == playerId {
			return nil
		} else if backer3ID == playerId {
			return nil
		} else if player2ID == playerId {
			return fmt.Errorf("error - this player does not have prize")
		}
		return fmt.Errorf("no such player %s", playerId)
	}
	copy = successCase14
	actor = &copy
	actor.mfundPlayer = func(playerId string, amount uint) error {
		return fmt.Errorf("db error")
	}
	actor.mrollback = func() error {
		return nil
	}
	rr = serveHTTP("POST", "http://localhost:8080/resultTournament", resultJSON(&result1), actor, resultTournamentHandler, t)
	if rr.Code != http.StatusInternalServerError || actor.isAllExpectedCall() == false {
		t.Errorf("handler returned wrong status code: got %v want %v",
			rr.Code, http.StatusInternalServerError)
	}
	// 15 request second call fundPlayer return db error
	copy = successCase14
	actor = &copy
	actor.mfundPlayer = func(playerId string, amount uint) error {
		if amount != eachWinnerPrize {
			return fmt.Errorf("error - amount!=eachWinnerPrize")
		}
		if player1ID == playerId {
			return nil
		} else if backer1ID == playerId {
			return fmt.Errorf("db error")
		} else if backer2ID == playerId {
			return nil
		} else if backer3ID == playerId {
			return nil
		} else if player2ID == playerId {
			return fmt.Errorf("error - this player does not have prize")
		}
		return fmt.Errorf("no such player %s", playerId)
	}
	actor.mrollback = func() error {
		return nil
	}
	rr = serveHTTP("POST", "http://localhost:8080/resultTournament", resultJSON(&result1), actor, resultTournamentHandler, t)
	if rr.Code != http.StatusInternalServerError || actor.isAllExpectedCall() == false {
		t.Errorf("handler returned wrong status code: got %v want %v",
			rr.Code, http.StatusInternalServerError)
	}

	// 16 request closeTournament return db error
	copy = successCase14
	actor = &copy
	actor.mcloseTournament = func(tourmID string) error {
		return fmt.Errorf("db error")
	}
	actor.mrollback = func() error {
		return nil
	}

	rr = serveHTTP("POST", "http://localhost:8080/resultTournament", resultJSON(&result1), actor, resultTournamentHandler, t)
	if rr.Code != http.StatusInternalServerError || actor.isAllExpectedCall() == false {
		t.Errorf("handler returned wrong status code: got %v want %v",
			rr.Code, http.StatusInternalServerError)
	}
}

func getResult(r io.Reader) []struct {
	PlayerId string `json:"playerId"`
	Balance  string `json:"balance"`
} {
	results := make([]struct {
		PlayerId string `json:"playerId"`
		Balance  string `json:"balance"`
	}, 0, 10)

	if b, err := ioutil.ReadAll(r); err == nil {
		if err = json.Unmarshal(b, &results); err == nil {
			return results
		} else {
			fmt.Println(err)
		}
	}
	return nil
}

func TestPlayerBalanceHandler(t *testing.T) {
	playerID := "sara"
	playerBalance := uint(100)
	player2ID := "john"
	player2Balance := uint(200)
	player3ID := "someName"
	player3Balance := uint(300)
	actor := &dbActorMock{}
	actor.mgetPlayerBalance = func(playerId string) (uint, error) {
		if playerID == playerId {
			return playerBalance, nil
		} else if player2ID == playerId {
			return player2Balance, nil
		} else if player3ID == playerId {
			return player3Balance, nil
		}
		return 0, sql.ErrNoRows
	}

	rr := serveHTTP("GET", "http://localhost:8080/fund?playerId=sara&playerId=john&playerId=someName", nil, actor, playerBalanceHandler, t)
	if rr.Code != http.StatusOK || actor.isAllExpectedCall() == false {
		t.Errorf("handler returned wrong status code: got %v want %v",
			rr.Code, http.StatusOK)
	}
	results := getResult(rr.Body)
	if results[0].PlayerId == playerID && results[0].Balance == strconv.Itoa(int(playerBalance)) &&
		results[1].PlayerId == player2ID && results[1].Balance == strconv.Itoa(int(player2Balance)) &&
		results[2].PlayerId == player3ID && results[2].Balance == strconv.Itoa(int(player3Balance)) {
		// ok
	} else {
		t.Errorf("balance result is not equal source")
	}

	// 2 request case when no player2ID
	actor = &dbActorMock{}
	actor.mgetPlayerBalance = func(playerId string) (uint, error) {
		if playerID == playerId {
			return playerBalance, nil
		} else if player2ID == playerId {
			return 0, sql.ErrNoRows
		} else if player3ID == playerId {
			return player3Balance, nil
		}
		return 0, sql.ErrNoRows
	}

	rr = serveHTTP("GET", "http://localhost:8080/fund?playerId=sara&playerId=john&playerId=someName", nil, actor, playerBalanceHandler, t)
	if rr.Code != http.StatusBadRequest || actor.isAllExpectedCall() == false {
		t.Errorf("handler returned wrong status code: got %v want %v",
			rr.Code, http.StatusBadRequest)
	}

	// 3 request case when balance player2ID returned sql error
	actor = &dbActorMock{}
	actor.mgetPlayerBalance = func(playerId string) (uint, error) {
		if playerID == playerId {
			return playerBalance, nil
		} else if player2ID == playerId {
			return 0, fmt.Errorf("some db error")
		} else if player3ID == playerId {
			return player3Balance, nil
		}
		return 0, sql.ErrNoRows
	}

	rr = serveHTTP("GET", "http://localhost:8080/fund?playerId=sara&playerId=john&playerId=someName", nil, actor, playerBalanceHandler, t)
	if rr.Code != http.StatusInternalServerError || actor.isAllExpectedCall() == false {
		t.Errorf("handler returned wrong status code: got %v want %v",
			rr.Code, http.StatusInternalServerError)
	}
}

/*
func TestAll(t *testing.T) {
	TestPlayerBalanceHandler(t)
	TestFundPlayerHandler(t)
	TestTakePlayerHandler(t)
	TestAnnounceTournamentHandler(t)
	TestJoinTournamentHandler(t)
	TestResultTournamentHandler(t)

}
*/
