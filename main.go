package main

import (
	"database/sql"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"strconv"
	"time"

	_ "github.com/go-sql-driver/mysql"
)

var createChema = "CREATE SCHEMA tournament ;"
var useDB = "USE tournament;"
var dropPlayers = "DROP TABLE tournament.players;"
var dropTournaments = "DROP TABLE tournament.tournaments;"
var dropPlayerInTournaments = "DROP TABLE tournament.players_in_tournaments;"

var createTableplayers = `
CREATE TABLE players (
  playerid VARCHAR(125) NOT NULL,
  points INT NOT NULL,
  PRIMARY KEY (playerid),
  UNIQUE INDEX id_UNIQUE (playerid ASC));
`

var createTableTournaments = `CREATE TABLE tournaments (
  idtournaments VARCHAR(125) NOT NULL,
  deposit INT UNSIGNED NOT NULL,
  state VARCHAR(2) NOT NULL DEFAULT 's',
  PRIMARY KEY (idtournaments),
  UNIQUE INDEX idtournaments_UNIQUE (idtournaments ASC));
`

var createTablePlayersInTournaments = `CREATE TABLE players_in_tournaments (
  linkid INT UNSIGNED NOT NULL AUTO_INCREMENT,
  playerid VARCHAR(125) NOT NULL,
  bakerid VARCHAR(125) NOT NULL,
  tournamentid VARCHAR(125) NOT NULL,
  type VARCHAR(2) NOT NULL,
  stake INT UNSIGNED NOT NULL,
  profit INT UNSIGNED,
  PRIMARY KEY (linkid),
  UNIQUE INDEX linkid_UNIQUE (linkid ASC),
  UNIQUE INDEX fk_link_entry (playerid ASC, tournamentid ASC,bakerid ASC),
  INDEX fk_tournaments_idx (tournamentid ASC),
  INDEX fk_players_idx (playerid ASC),
  CONSTRAINT fk_tournaments
    FOREIGN KEY (tournamentid)
    REFERENCES tournament.tournaments (idtournaments)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_players
    FOREIGN KEY (playerid)
    REFERENCES tournament.players (playerid)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
`

//type - b - baker p - player

func main() {
	var dbName string
	var username string
	var password string
	var port int
	var dbPort int
	var dbHost string

	flag.StringVar(&dbName, "dbname", "tournament", "data base name")
	flag.StringVar(&username, "dblogin", "tournamentservice", "database username")
	flag.StringVar(&password, "dbpasswd", "tournamentservpwd", "database name password")
	flag.IntVar(&dbPort, "dbport", 3306, "database port")
	flag.StringVar(&dbHost, "dbhost", "localhost", "database hostname")
	flag.IntVar(&port, "port", 8080, "port")

	flag.Parse()

	log.Printf("running with parameters dbname:%s,dbport:%d,dbhost:%s,dbusername:%s,dbpasswordlen:%d,listenport:%d", dbName, dbPort, dbHost, username, len(password), port)

	if db, err := sql.Open("mysql", fmt.Sprintf("%s:%s@tcp(%s:%d)/%s", username, password, dbHost, dbPort, dbName)); err != nil {
		log.Fatalf("can't open database: %s", err.Error())
	} else {
		defer db.Close()
		ListenAndServe(port, db, NewStatement)
		log.Println("Tournament service exiting ...")
	}
}

func ListenAndServe(port int, db *sql.DB, stmtBuilder StatementBuilder) {

	var err error
	isPinged := false
	for i := 0; i < 10; i++ {
		log.Printf("try ping ... ")
		if err = db.Ping(); err != nil {
			log.Printf("can't ping db: %s", err.Error())
		} else {
			isPinged = true
			break
		}
		time.Sleep(time.Second * 2)
	}

	if isPinged == false {
		log.Fatalln("can't ping db ... ")
	}
	if _, err := db.Exec("USE tournament;"); err != nil {
		log.Fatalf("can't USE tournament db: %s", err.Error())
	}

	log.Println("try create tables...")
	db.Exec(createTableplayers)
	db.Exec(createTableTournaments)
	db.Exec(createTablePlayersInTournaments)

	log.Println("try create statements...")
	dbActor, err := newDbActor(db, stmtBuilder)
	if err != nil {
		log.Fatalf("can't create dbActor: %s", err.Error())
	}

	log.Println("register handlers...")
	http.HandleFunc("/fund", func(w http.ResponseWriter, r *http.Request) {
		procHandler(fundHandler, w, r, "fundHandler", dbActor)
	})
	http.HandleFunc("/take", func(w http.ResponseWriter, r *http.Request) {
		procHandler(takeHandler, w, r, "/takeHandler", dbActor)
	})
	http.HandleFunc("/announceTournament", func(w http.ResponseWriter, r *http.Request) {
		procHandler(announceTournamentHandler, w, r, "/announceTournament", dbActor)
	})
	http.HandleFunc("/joinTournament", func(w http.ResponseWriter, r *http.Request) {
		procHandler(joinTournamentHandler, w, r, "/joinTournament", dbActor)
	})
	http.HandleFunc("/resultTournament", func(w http.ResponseWriter, r *http.Request) {
		if r.Method != "POST" {
			http.Error(w, "only POST method supported", http.StatusBadRequest)
		} else {
			procHandler(resultTournamentHandler, w, r, "/resultTournament", dbActor)
		}
	})

	http.HandleFunc("/reset", func(w http.ResponseWriter, r *http.Request) {
		resetHandler(w, r, db)
	})

	http.HandleFunc("/balance", func(w http.ResponseWriter, r *http.Request) {
		playerBalanceHandler(w, r, dbActor)
	})
	addr := fmt.Sprintf(":%d", port)
	log.Printf("try listen %s...", addr)

	http.ListenAndServe(addr, nil)
}

func procHandler(hdl func(http.ResponseWriter, *http.Request, dbActor) (retCode int, errResp error), w http.ResponseWriter, r *http.Request, handlerName string, db dbActor) {
	defer func() {
		if rec := recover(); rec != nil {
			err := fmt.Sprintf("exception in handler %s: %s", handlerName, rec)
			log.Print(err)
			http.Error(w, err, http.StatusInternalServerError)
		}
	}()

	var code int
	var err error
	code, err = hdl(w, r, db)
	switch {
	case code >= 500:
		log.Printf("%s internal server error: %s", handlerName, err.Error())
		http.Error(w, err.Error(), code)
	case code >= 400 && code < 500:
		http.Error(w, err.Error(), code)
	default:
		w.WriteHeader(code)
		if err != nil {
			w.Write([]byte(err.Error()))
		}
	}

}

func fundHandler(w http.ResponseWriter, r *http.Request, db dbActor) (int, error) {
	v := r.URL.Query()
	playerIDStr := v.Get("playerId")
	pointsStr := v.Get("points")
	if playerIDStr != "" && pointsStr != "" {
		if points, err := strconv.Atoi(pointsStr); err != nil {
			return http.StatusBadRequest, fmt.Errorf("can't convert points to integer %s %s", pointsStr, err.Error())
		} else {
			if points < 0 {
				return http.StatusBadRequest, fmt.Errorf("points must be positive %s", pointsStr)
			}
			if exist, err := db.isPlayerExist(playerIDStr); err != nil && err != sql.ErrNoRows {
				return http.StatusInternalServerError, fmt.Errorf("can't check player presense %s %s", pointsStr, err.Error())
			} else {
				if exist {
					if err = db.fundPlayer(playerIDStr, uint(points)); err != nil {
						return http.StatusInternalServerError, err
					}
				} else {
					if err = db.addPlayer(playerIDStr, uint(points)); err != nil {
						return http.StatusInternalServerError, err
					}
				}
				return http.StatusOK, nil
			}
		}
	} else {
		return http.StatusBadRequest, fmt.Errorf("not enough parameters playerId = %s points = %s", playerIDStr, pointsStr)
	}
}

func takeHandler(w http.ResponseWriter, r *http.Request, db dbActor) (int, error) {
	v := r.URL.Query()
	playerIDStr := v.Get("playerId")
	pointsStr := v.Get("points")
	if playerIDStr != "" && pointsStr != "" {
		if points, err := strconv.Atoi(pointsStr); err != nil {
			return http.StatusBadRequest, fmt.Errorf("can't convert points to integer %s %s", pointsStr, err.Error())
		} else {
			if points < 0 {
				return http.StatusBadRequest, fmt.Errorf("points must be positive %s", pointsStr)
			}
			if exist, err := db.isPlayerExist(playerIDStr); err != nil && err != sql.ErrNoRows {
				return http.StatusInternalServerError, fmt.Errorf("can't check player presense %s %s", pointsStr, err.Error())
			} else {
				if exist {
					if balance, err := db.getPlayerBalance(playerIDStr); err == nil {
						if balance >= uint(points) {
							if err := db.takePlayer(playerIDStr, uint(points)); err == nil {
								return http.StatusOK, nil
							} else {
								return http.StatusInternalServerError, fmt.Errorf("can't take funds from player: %s error: %s", playerIDStr, err.Error())
							}
						} else {
							return http.StatusBadRequest, fmt.Errorf("not enough funds from player: %s balance: %d", playerIDStr, balance)
						}
					} else {
						return http.StatusInternalServerError, fmt.Errorf("can't take palance from player: %s error: %s", playerIDStr, err.Error())
					}
				} else {
					return http.StatusBadRequest, fmt.Errorf("player does'not exists: %s", playerIDStr)
				}
			}
		}
	} else {
		return http.StatusBadRequest, fmt.Errorf("not enough parameters playerId = %s points = %s", playerIDStr, pointsStr)
	}
}

func announceTournamentHandler(w http.ResponseWriter, r *http.Request, db dbActor) (int, error) {
	v := r.URL.Query()
	tournamentId := v.Get("tournamentId")
	depositstr := v.Get("deposit")
	var deposit uint
	if tournamentId == "" || depositstr == "" {
		return http.StatusBadRequest, fmt.Errorf("tournamentId and deposit are both required")
	}
	if depositConv, err := strconv.Atoi(depositstr); err != nil || depositConv < 0 {
		var errorStr string
		if err != nil {
			errorStr = err.Error()
		} else {
			errorStr = "deposit less than zero"
		}
		return http.StatusBadRequest, fmt.Errorf("tournamentId is must be decimical and bigger than zero :tournamentId = %s, convert error -%s", tournamentId, errorStr)
	} else {
		deposit = uint(depositConv)
	}
	_, err := db.tournamentState(tournamentId)
	switch {
	case err == nil:
		return http.StatusBadRequest, fmt.Errorf("tournament with tournamentId %s is is already exist", tournamentId)
	case err != sql.ErrNoRows:
		return http.StatusInternalServerError, fmt.Errorf("get tournament state failed %s", err.Error())
	}
	if err := db.announceTournament(tournamentId, deposit); err != nil {
		return http.StatusInternalServerError, fmt.Errorf("can't announce tournament %s, error - %s", tournamentId, err.Error())
	}
	return http.StatusOK, nil
}

func joinTournamentHandler(w http.ResponseWriter, r *http.Request, db dbActor) (retCode int, retErr error) {
	v := r.URL.Query()
	tournamentId := v.Get("tournamentId")
	if tournamentId == "" {
		return http.StatusBadRequest, fmt.Errorf("tournament id is absent")
	}

	playerId := v.Get("playerId")
	if playerId == "" {
		return http.StatusBadRequest, fmt.Errorf("playerId is absent %s", playerId)
	}

	tx, err := db.beginTransaction()
	if err != nil {
		return http.StatusInternalServerError, fmt.Errorf("can't start transaction: %s", err.Error())
	}

	success := func(code int, err error) (int, error) {
		if tx != nil {
			if err := tx.commit(); err != nil {
				tx.rollback()
				tx = nil
				return http.StatusInternalServerError, fmt.Errorf("can't Commit changes: %s", err.Error())
			}
			tx = nil
		}
		return code, err
	}
	fail := func(code int, err error) (int, error) {
		if tx != nil {
			tx.rollback()
			tx = nil
		}
		return code, err
	}
	defer func() {
		if rec := recover(); rec != nil {
			retCode = http.StatusInternalServerError
			retErr = fmt.Errorf("exception in joinTournamentHandler %s", rec)
		}
		// changes MUST be commited and tx MUST be nil. If othervise - something going wrong.
		if tx != nil {
			tx.rollback()
			tx = nil
		}
	}()

	deposit, tourmState, err := tx.getTournament(tournamentId)
	switch {
	case err == sql.ErrNoRows:
		return fail(http.StatusBadRequest, fmt.Errorf("no tournament with such Id %d", tournamentId))
	case err != nil:
		return fail(http.StatusInternalServerError, fmt.Errorf("get tournament failed %s", err.Error()))
	case tourmState != "s":
		return fail(http.StatusBadRequest, fmt.Errorf("tournament is end %d  (with status %s)", tournamentId, tourmState))
	}

	_, err = tx.isPlayerExist(playerId)
	switch {
	case err == sql.ErrNoRows:
		return fail(http.StatusBadRequest, fmt.Errorf("player does not exist %s", playerId))
	case err != nil:
		return fail(http.StatusInternalServerError, fmt.Errorf("can't get player balance %s", err.Error()))
	}
	// need calculate deposit divided by bakers plus players count
	bakersAndPlayers := 1
	for _, backerID := range v["backerId"] {
		if exist, err := tx.isPlayerExist(backerID); err == nil && exist == true {
			bakersAndPlayers++
		} else {
			if err != nil && err != sql.ErrNoRows {
				return fail(http.StatusInternalServerError, fmt.Errorf("can't check backer does not exist %s", backerID))
			}
			return fail(http.StatusBadRequest, fmt.Errorf("backer does not exist %s", backerID))
		}
	}
	deposit = uint(deposit / uint(bakersAndPlayers))

	playerBalance, err := tx.getPlayerBalance(playerId)
	switch {
	case err == sql.ErrNoRows:
		return fail(http.StatusBadRequest, fmt.Errorf("no player with such Id %s", playerId))
	case err != nil:
		return fail(http.StatusInternalServerError, fmt.Errorf("get player balance fails %s, error - %s", playerId, err.Error()))
	case playerBalance < deposit:
		return fail(http.StatusBadRequest, fmt.Errorf("player does't have mush funds %s %d deposit: %d", playerId, playerBalance, deposit))
	}
	if err := tx.takePlayer(playerId, deposit); err != nil {
		return fail(http.StatusInternalServerError, fmt.Errorf("take player deposit fails %s,deposit %d error - %s", playerId, deposit, err.Error()))
	}

	if err := tx.joinTournament(tournamentId, playerId, "p", "", deposit); err != nil {
		return fail(http.StatusInternalServerError, fmt.Errorf("player %s can't join to tournament: error - %s", playerId, err.Error()))
	}

	for _, backerID := range v["backerId"] {
		balance, err := tx.getPlayerBalance(backerID)
		switch {
		case err != nil:
			return fail(http.StatusInternalServerError, fmt.Errorf("can't get balance %s error %s", backerID, err.Error()))
		case balance < deposit:
			return fail(http.StatusBadRequest, fmt.Errorf("baker %s does't have mush funds %d", backerID, balance))
		}
		if err := tx.takePlayer(backerID, deposit); err != nil {
			return fail(http.StatusInternalServerError, fmt.Errorf("can't take deposit %s error %s", backerID, err.Error()))
		}
		if err := tx.joinTournament(tournamentId, playerId, "b", backerID, deposit); err != nil {
			return fail(http.StatusInternalServerError, fmt.Errorf("baker %s can't join to tournament: error - %s", backerID, err.Error()))
		}
	}

	return success(http.StatusOK, nil)
}

type resultTournament struct {
	TournamentId string `json:"tournamentId"`
	Winners      []struct {
		PlayerId string `json:"playerId"`
		Prize    uint   `json:"prize"`
	} `json:"winners"`
}

func (th *resultTournament) addWinner(playerID string, prize uint) {
	var winner struct {
		PlayerId string `json:"playerId"`
		Prize    uint   `json:"prize"`
	}
	winner.PlayerId = playerID
	winner.Prize = prize
	th.Winners = append(th.Winners, winner)
}

func (th resultTournament) prize() (result uint) {
	for _, w := range th.Winners {
		result += w.Prize
	}
	return
}

type keyPlayerBaker struct {
	player string
	baker  string
}

func resultTournamentHandler(w http.ResponseWriter, r *http.Request, db dbActor) (retCode int, retErr error) {
	decoder := json.NewDecoder(r.Body)
	var results resultTournament
	err := decoder.Decode(&results)
	defer r.Body.Close()
	if err != nil {
		return http.StatusBadRequest, fmt.Errorf("can't parse json %s", err.Error())
	}

	tx, err := db.beginTransaction()
	if err != nil {
		return http.StatusInternalServerError, fmt.Errorf("can't start transaction")
	}
	success := func(code int, err error) (int, error) {
		if tx != nil {
			if err := tx.commit(); err != nil {
				tx.rollback()
				tx = nil
				return http.StatusInternalServerError, fmt.Errorf("can't Commit changes: %s", err.Error())
			}
			tx = nil
		}
		return code, err
	}
	fail := func(code int, err error) (int, error) {
		if tx != nil {
			tx.rollback()
			tx = nil
		}
		return code, err
	}
	defer func() {
		if rec := recover(); rec != nil {
			retCode = http.StatusInternalServerError
			retErr = fmt.Errorf("exception in resultTournamentHandler %s", rec)
		}
		// changes MUST be commited and tx MUST be nil. If other - something going wrong...
		if tx != nil {
			tx.rollback()
			tx = nil
		}
	}()

	if _, state, err := tx.getTournament(results.TournamentId); err != nil {
		if err == sql.ErrNoRows {
			return fail(http.StatusBadRequest, fmt.Errorf("can't get tournament %d state %s", results.TournamentId, err.Error()))
		}
		return fail(http.StatusInternalServerError, fmt.Errorf("can't get tournament %d state %s", results.TournamentId, err.Error()))
	} else {
		if state != "s" {
			return fail(http.StatusBadRequest, fmt.Errorf("tournament closed %d", results.TournamentId))
		}
	}

	tourmTotal, err := tx.getTournamentTotalStake(results.TournamentId)
	switch {
	case err == sql.ErrNoRows:
		return fail(http.StatusBadRequest, fmt.Errorf("no joined to tournament players ..."))
	case err != nil:
		return fail(http.StatusInternalServerError, fmt.Errorf("can't get total prize: %s", err.Error()))
	case tourmTotal != results.prize():
		return fail(http.StatusBadRequest, fmt.Errorf("total prize not equal baker and player stake total prize: %d stake: %d", results.prize(), tourmTotal))
	}

	prizes := make([]struct {
		player string
		baker  string
		prize  uint
	}, 0, 10)
	prizesPtr := &prizes

	for _, winner := range results.Winners {
		if totalByWinner, err := tx.getTournamentTotalPlayerStake(results.TournamentId, winner.PlayerId); err != nil {
			if err == sql.ErrNoRows {
				return fail(http.StatusBadRequest, fmt.Errorf("winner was not joined to tournament tournament: %s and playerId %s", results.TournamentId, winner.PlayerId))
			}
			return fail(http.StatusInternalServerError, fmt.Errorf("can't calc total prize for tournamentId: %s and playerId %s", results.TournamentId, winner.PlayerId))
		} else {
			if err := tx.getTournamentPlayerBakers(results.TournamentId, winner.PlayerId, func(linkid uint, playerId string, bakerType string, bakerid string, stake uint) bool {
				calc := float32(stake) / float32(totalByWinner) * float32(winner.Prize)
				var prize struct {
					player string
					baker  string
					prize  uint
				}
				prize.player = winner.PlayerId
				prize.baker = bakerid
				prize.prize = uint(calc)
				(*prizesPtr) = append((*prizesPtr), prize)
				return true
			}); err != nil {
				return fail(http.StatusInternalServerError, fmt.Errorf("can't get tournament player bakers %s %s error %s", results.TournamentId, winner.PlayerId, err.Error()))
			}
		}
	}

	for _, prize := range prizes {
		if prize.baker != "" {
			if err := tx.fundPlayer(prize.baker, prize.prize); err != nil {
				return fail(http.StatusInternalServerError, fmt.Errorf("can't fundPlayer %s", err.Error()))
			}
		} else {
			if err := tx.fundPlayer(prize.player, prize.prize); err != nil {
				return fail(http.StatusInternalServerError, fmt.Errorf("can't fundPlayer %s", err.Error()))
			}
		}
	}

	if err := tx.closeTournament(results.TournamentId); err != nil {
		return fail(http.StatusInternalServerError, fmt.Errorf("can't close tournament %s", err.Error()))
	}
	return success(http.StatusOK, nil)
}

func resetHandler(w http.ResponseWriter, r *http.Request, db *sql.DB) (retCode int, retErr error) {
	if _, err := db.Exec(dropPlayerInTournaments); err != nil {
		io.WriteString(w, fmt.Sprintf("drop fail: %s\n", err.Error()))
	}
	if _, err := db.Exec(dropTournaments); err != nil {
		io.WriteString(w, fmt.Sprintf("drop fail: %s\n", err.Error()))
	}
	if _, err := db.Exec(dropPlayers); err != nil {
		io.WriteString(w, fmt.Sprintf("drop fail: %s\n", err.Error()))
	}
	if _, err := db.Exec(createChema); err != nil {
		io.WriteString(w, fmt.Sprintf("create schema fail: %s\n", err.Error()))
	}
	if _, err := db.Exec(useDB); err != nil {
		io.WriteString(w, fmt.Sprintf("use db fail: %s\n", err.Error()))
	}

	if _, err := db.Exec(createTableplayers); err != nil {
		io.WriteString(w, fmt.Sprintf("create table players fail: %s\n", err.Error()))
	}
	if _, err := db.Exec(createTableTournaments); err != nil {
		io.WriteString(w, fmt.Sprintf("create table Tournaments fail: %s\n", err.Error()))
	}
	if _, err := db.Exec(createTablePlayersInTournaments); err != nil {
		io.WriteString(w, fmt.Sprintf("create table PlayersInTournaments fail: %s\n", err.Error()))
	}

	return http.StatusOK, nil
}

func playerBalanceHandler(w http.ResponseWriter, r *http.Request, db dbActor) (retCode int, retErr error) {
	retCode = http.StatusOK
	retErr = nil
	v := r.URL.Query()
	playerBalanceMap := make(map[string]string)
	for _, playerId := range v["playerId"] {
		if balance, err := db.getPlayerBalance(playerId); err == nil {
			playerBalanceMap[playerId] = strconv.Itoa(int(balance))
		} else {
			if err == sql.ErrNoRows {
				return http.StatusBadRequest, fmt.Errorf("player %s absent", playerId)
			}
			return http.StatusInternalServerError, fmt.Errorf("internal error")
		}
	}
	idCount := len(playerBalanceMap)
	if idCount > 1 {
		io.WriteString(w, "[\n")
	}
	index := 0
	for playerId, balance := range playerBalanceMap {
		io.WriteString(w, fmt.Sprintf("{\"playerId\": \"%s\", \"balance\": \"%s\"}", playerId, balance))
		if index < (idCount - 1) {
			io.WriteString(w, fmt.Sprintf(",\n"))
		}
		index++
	}
	if idCount > 1 {
		io.WriteString(w, "\n]")
	}

	w.Header().Set("Content-Type", "application/json")

	return http.StatusOK, nil
}
