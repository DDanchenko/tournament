package main

type dbActorMock struct {
	misPlayerExist                 func(playerId string) (bool, error)
	mgetPlayerBalance              func(playerId string) (uint, error)
	maddPlayer                     func(playerId string, amount uint) error
	mfundPlayer                    func(playerId string, amount uint) error
	mtakePlayer                    func(playerId string, amount uint) error
	mannounceTournament            func(tornamentId string, deposit uint) error
	mtournamentState               func(tornamentId string) (byte, error)
	mgetTournament                 func(tornamentId string) (uint, string, error)
	mcloseTournament               func(tornamentId string) error
	mjoinTournament                func(tornamentId string, playerId string, joinerType string, bakerId string, stake uint) error
	mgetTournamentPlayerBakers     func(tornamentId string, playerId string, inject func(linkid uint, playerId string, bakerType string, bakerid string, stake uint) bool) (resErr error)
	mgetTournamentTotalStake       func(tornamentId string) (total uint, err error)
	mgetTournamentTotalPlayerStake func(tornamentId string, playerId string) (total uint, err error)
	mbeginTransaction              func() (dbTransActor, error)
	mcommit                        func() error
	mrollback                      func() error

	bisPlayerExist                 bool
	bgetPlayerBalance              bool
	baddPlayer                     bool
	bfundPlayer                    bool
	btakePlayer                    bool
	bannounceTournament            bool
	btournamentState               bool
	bgetTournament                 bool
	bcloseTournament               bool
	bjoinTournament                bool
	bgetTournamentPlayerBakers     bool
	bgetTournamentTotalStake       bool
	bgetTournamentTotalPlayerStake bool
	bbeginTransaction              bool
	bcommit                        bool
	brollback                      bool
}

func (mock *dbActorMock) isAllExpectedCall() bool {
	if mock.bisPlayerExist == false && mock.misPlayerExist != nil {
		return false
	}
	if mock.bgetPlayerBalance == false && mock.mgetPlayerBalance != nil {
		return false
	}
	if mock.baddPlayer == false && mock.maddPlayer != nil {
		return false
	}
	if mock.bfundPlayer == false && mock.mfundPlayer != nil {
		return false
	}
	if mock.btakePlayer == false && mock.mtakePlayer != nil {
		return false
	}
	if mock.bannounceTournament == false && mock.mannounceTournament != nil {
		return false
	}
	if mock.btournamentState == false && mock.mtournamentState != nil {
		return false
	}
	if mock.bgetTournament == false && mock.mgetTournament != nil {
		return false
	}
	if mock.bcloseTournament == false && mock.mcloseTournament != nil {
		return false
	}
	if mock.bjoinTournament == false && mock.mjoinTournament != nil {
		return false
	}
	if mock.bgetTournamentPlayerBakers == false && mock.mgetTournamentPlayerBakers != nil {
		return false
	}
	if mock.bgetTournamentTotalStake == false && mock.mgetTournamentTotalStake != nil {
		return false
	}
	if mock.bgetTournamentTotalPlayerStake == false && mock.mgetTournamentTotalPlayerStake != nil {
		return false
	}
	if mock.bbeginTransaction == false && mock.mbeginTransaction != nil {
		return false
	}
	if mock.bcommit == false && mock.mcommit != nil {
		return false
	}
	if mock.brollback == false && mock.mrollback != nil {
		return false
	}
	return true
}

func (mock *dbActorMock) reset() {
	mock.bisPlayerExist = false
	mock.bgetPlayerBalance = false
	mock.baddPlayer = false
	mock.bfundPlayer = false
	mock.btakePlayer = false
	mock.bannounceTournament = false
	mock.btournamentState = false
	mock.bgetTournament = false
	mock.bcloseTournament = false
	mock.bjoinTournament = false
	mock.bgetTournamentPlayerBakers = false
	mock.bgetTournamentTotalStake = false
	mock.bgetTournamentTotalPlayerStake = false
	mock.bbeginTransaction = false
	mock.bcommit = false
	mock.brollback = false
}

func (mock *dbActorMock) commit() error {
	if mock.mcommit != nil {
		mock.bcommit = true
		return mock.mcommit()
	}
	panic("no mock method")
}

func (mock *dbActorMock) rollback() error {
	if mock.mrollback != nil {
		mock.brollback = true
		return mock.mrollback()
	}
	panic("no mock method")
}

func (mock *dbActorMock) isPlayerExist(playerId string) (bool, error) {
	if mock.misPlayerExist != nil {
		mock.bisPlayerExist = true
		return mock.misPlayerExist(playerId)
	}
	panic("no mock method")

}
func (mock *dbActorMock) getPlayerBalance(playerId string) (uint, error) {
	if mock.mgetPlayerBalance != nil {
		mock.bgetPlayerBalance = true
		return mock.mgetPlayerBalance(playerId)
	}
	panic("no mock method")
}
func (mock *dbActorMock) addPlayer(playerId string, amount uint) error {
	if mock.maddPlayer != nil {
		mock.baddPlayer = true
		return mock.maddPlayer(playerId, amount)
	}
	panic("no mock method")
}

func (mock *dbActorMock) fundPlayer(playerId string, amount uint) error {
	if mock.mfundPlayer != nil {
		mock.bfundPlayer = true
		return mock.mfundPlayer(playerId, amount)
	}
	panic("no mock method")
}
func (mock *dbActorMock) takePlayer(playerId string, amount uint) error {
	if mock.mtakePlayer != nil {
		mock.btakePlayer = true
		return mock.mtakePlayer(playerId, amount)
	}
	panic("no mock method")
}
func (mock *dbActorMock) announceTournament(tornamentId string, deposit uint) error {
	if mock.mannounceTournament != nil {
		mock.bannounceTournament = true
		return mock.mannounceTournament(tornamentId, deposit)
	}
	panic("no mock method")
}
func (mock *dbActorMock) tournamentState(tornamentId string) (byte, error) {
	if mock.mtournamentState != nil {
		mock.btournamentState = true
		return mock.mtournamentState(tornamentId)
	}
	panic("no mock method")
}
func (mock *dbActorMock) getTournament(tornamentId string) (uint, string, error) {
	if mock.mgetTournament != nil {
		mock.bgetTournament = true
		return mock.mgetTournament(tornamentId)
	}
	panic("no mock method")
}
func (mock *dbActorMock) closeTournament(tornamentId string) error {
	if mock.mgetTournament != nil {
		mock.bcloseTournament = true
		return mock.mcloseTournament(tornamentId)
	}
	panic("no mock method")
}
func (mock *dbActorMock) joinTournament(tornamentId string, playerId string, joinerType string, bakerId string, stake uint) error {
	if mock.mjoinTournament != nil {
		mock.bjoinTournament = true
		return mock.mjoinTournament(tornamentId, playerId, joinerType, bakerId, stake)
	}
	panic("no mock method")
}
func (mock *dbActorMock) getTournamentPlayerBakers(tornamentId string, playerId string, inject func(linkid uint, playerId string, bakerType string, bakerid string, stake uint) bool) (resErr error) {
	if mock.mgetTournamentPlayerBakers != nil {
		mock.bgetTournamentPlayerBakers = true
		return mock.mgetTournamentPlayerBakers(tornamentId, playerId, inject)
	}
	panic("no mock method")
}
func (mock *dbActorMock) getTournamentTotalStake(tornamentId string) (total uint, err error) {
	if mock.mgetTournamentTotalStake != nil {
		mock.bgetTournamentTotalStake = true
		return mock.mgetTournamentTotalStake(tornamentId)
	}
	panic("no mock method")
}
func (mock *dbActorMock) getTournamentTotalPlayerStake(tornamentId string, playerId string) (total uint, err error) {
	if mock.mgetTournamentTotalPlayerStake != nil {
		mock.bgetTournamentTotalPlayerStake = true
		return mock.mgetTournamentTotalPlayerStake(tornamentId, playerId)
	}
	panic("no mock method")
}
func (mock *dbActorMock) beginTransaction() (dbTransActor, error) {
	if mock.mbeginTransaction != nil {
		mock.bbeginTransaction = true
		return mock.mbeginTransaction()
	}
	panic("no mock method")
}
