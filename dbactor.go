package main

import (
	"database/sql"
	"fmt"
)

// tournament: s-started,c-closed
// player in tournament:

var addPlayerQuery = "INSERT INTO players (playerid,points) VALUES (?,?);"
var balancePlayerQuery = "SELECT points FROM players WHERE playerid=?;"
var fundPlayerQuery = "UPDATE players SET players.points=players.points+? WHERE players.playerid=?;"
var takePlayerQuery = "UPDATE players SET players.points=players.points-? WHERE players.playerid=?;"
var announceTournamentQuery = "INSERT INTO tournaments (idtournaments,deposit,state) VALUES (?,?,'s');"
var closeTournamentQuery = "UPDATE tournaments SET tournaments.state='c' WHERE  tournaments.idtournaments=?;"
var tournamentStateQuery = "SELECT tournaments.state FROM tournaments WHERE tournaments.idtournaments=?;"
var joinTournamentQuery = "INSERT INTO players_in_tournaments (playerid,tournamentid,type,bakerid,stake) VALUES (?,?,?,?,?);"
var getTournamentQuery = "SELECT tournaments.deposit,tournaments.state FROM tournaments WHERE tournaments.idtournaments=?;"

var getTournamentPlayerBakersQuery = "SELECT linkid,playerid,type,bakerid,stake FROM players_in_tournaments WHERE players_in_tournaments.tournamentid=? AND players_in_tournaments.playerid=?;"
var getTournamentBakersQuery = "SELECT linkid,playerid,type,bakerid,stake FROM players_in_tournaments WHERE players_in_tournaments.tournamentid=?;"
var getTournamentTotalStakeQuery = "SELECT SUM(players_in_tournaments.stake) as totalStake FROM players_in_tournaments WHERE players_in_tournaments.tournamentid=?;"

var getTournamentTotalPLayerStakeQuery = "SELECT SUM(players_in_tournaments.stake) as totalStake FROM players_in_tournaments WHERE players_in_tournaments.tournamentid=? AND  players_in_tournaments.playerid=?;"

type dbActor interface {
	isPlayerExist(playerId string) (bool, error)
	getPlayerBalance(playerId string) (uint, error)
	addPlayer(playerId string, amount uint) error
	fundPlayer(playerId string, amount uint) error
	takePlayer(playerId string, amount uint) error
	announceTournament(tornamentId string, deposit uint) error
	tournamentState(tornamentId string) (byte, error)
	getTournament(tornamentId string) (uint, string, error)
	closeTournament(tornamentId string) error
	joinTournament(tornamentId string, playerId string, joinerType string, bakerId string, stake uint) error
	getTournamentPlayerBakers(tornamentId string, playerId string, inject func(linkid uint, playerId string, bakerType string, bakerid string, stake uint) bool) (resErr error)
	getTournamentTotalStake(tornamentId string) (total uint, err error)
	getTournamentTotalPlayerStake(tornamentId string, playerId string) (total uint, err error)
	//getTournamentBakers(tornamentId string, inject func(linkid uint, playerId string, bakerType string, bakerid string, stake uint) bool) (resErr error)
	beginTransaction() (dbTransActor, error)
}

type StatementBuilder func(string, *sql.DB) (IStatement, error)

type dbTransActor interface {
	dbActor
	rollback() error
	commit() error
}

func newDbActor(db *sql.DB, builder StatementBuilder) (dbActor, error) {
	var err error
	th := &dbactor{db: db, builder: builder}
	if th.addPlayerStmt, err = builder(addPlayerQuery, db); err != nil {
		return nil, fmt.Errorf("can't create statement %s, %s", addPlayerQuery, err.Error())
	}
	if th.balancePlayerStmt, err = builder(balancePlayerQuery, db); err != nil {
		return nil, fmt.Errorf("can't create statement %s, %s", balancePlayerQuery, err.Error())
	}
	if th.fundPlayerStmt, err = builder(fundPlayerQuery, db); err != nil {
		return nil, fmt.Errorf("can't create statement %s, %s", fundPlayerQuery, err.Error())
	}
	if th.takePlayerStmt, err = builder(takePlayerQuery, db); err != nil {
		return nil, fmt.Errorf("can't create statement %s, %s", takePlayerQuery, err.Error())
	}
	if th.announceTournamentStmt, err = builder(announceTournamentQuery, db); err != nil {
		return nil, fmt.Errorf("can't create statement %s, %s", announceTournamentQuery, err.Error())
	}
	if th.closeTournamentStmt, err = builder(closeTournamentQuery, db); err != nil {
		return nil, fmt.Errorf("can't create statement %s, %s", closeTournamentQuery, err.Error())
	}
	if th.tournamentStateStmt, err = builder(tournamentStateQuery, db); err != nil {
		return nil, fmt.Errorf("can't create statement %s, %s", tournamentStateQuery, err.Error())
	}
	if th.joinTournamentStmt, err = builder(joinTournamentQuery, db); err != nil {
		return nil, fmt.Errorf("can't create statement %s, %s", joinTournamentQuery, err.Error())
	}
	if th.getTournamentStmt, err = builder(getTournamentQuery, db); err != nil {
		return nil, fmt.Errorf("can't create statement %s, %s", getTournamentQuery, err.Error())
	}
	if th.getTournamentPlayerBakersStmt, err = builder(getTournamentPlayerBakersQuery, db); err != nil {
		return nil, fmt.Errorf("can't create statement %s, %s", getTournamentPlayerBakersQuery, err.Error())
	}
	if th.getTournamentBakersStmt, err = builder(getTournamentBakersQuery, db); err != nil {
		return nil, fmt.Errorf("can't create statement %s, %s", getTournamentBakersQuery, err.Error())
	}
	if th.getTournamentTotalStakeStmt, err = builder(getTournamentTotalStakeQuery, db); err != nil {
		return nil, fmt.Errorf("can't create statement %s, %s", getTournamentTotalStakeQuery, err.Error())
	}
	if th.getTournamentTotalPLayerStakeStmt, err = builder(getTournamentTotalPLayerStakeQuery, db); err != nil {
		return nil, fmt.Errorf("can't create statement %s, %s", getTournamentTotalPLayerStakeQuery, err.Error())
	}
	return th, nil
}

type dbactor struct {
	builder                StatementBuilder
	db                     *sql.DB
	addPlayerStmt          IStatement
	balancePlayerStmt      IStatement
	fundPlayerStmt         IStatement
	takePlayerStmt         IStatement
	announceTournamentStmt IStatement
	closeTournamentStmt    IStatement
	tournamentStateStmt    IStatement
	joinTournamentStmt     IStatement
	getTournamentStmt      IStatement

	getTournamentPlayerBakersStmt IStatement
	getTournamentBakersStmt       IStatement
	getTournamentTotalStakeStmt   IStatement

	getTournamentTotalPLayerStakeStmt IStatement

	tx       *sql.Tx
	txsource *dbactor
}

func (th *dbactor) commit() error {
	if th.tx == nil {
		return fmt.Errorf("actor is not transaction")
	}
	return th.tx.Commit()
}

func (th *dbactor) rollback() error {
	if th.tx == nil {
		return fmt.Errorf("actor is not transaction")
	}
	return th.tx.Rollback()
}

func (th *dbactor) beginTransaction() (dbTransActor, error) {
	var err error
	if th.tx == nil {
		newtr := &dbactor{txsource: th, db: th.db, builder: th.builder}
		if newtr.tx, err = th.db.Begin(); err != nil {
			return nil, err
		}
		return newtr, nil
	}
	return nil, fmt.Errorf("dbActor is already started transaction actor")
}

func (th *dbactor) isPlayerExist(playerId string) (bool, error) {
	var err error
	if th.balancePlayerStmt == nil {
		if th.txsource != nil && th.txsource.balancePlayerStmt != nil && th.tx != nil {
			if th.balancePlayerStmt, err = th.txsource.balancePlayerStmt.Transaction(th.tx); err != nil {
				return false, err
			}
		} else {
			return false, fmt.Errorf("db actor integrity fault: no tx source or source statement")
		}
	}

	var balance uint
	if err := th.balancePlayerStmt.QueryRow(playerId).Scan(&balance); err != nil {
		return false, err
	}
	return true, nil
}

func (th *dbactor) getPlayerBalance(playerId string) (uint, error) {
	var err error
	if th.balancePlayerStmt == nil {
		if th.txsource != nil && th.txsource.balancePlayerStmt != nil && th.tx != nil {
			if th.balancePlayerStmt, err = th.txsource.balancePlayerStmt.Transaction(th.tx); err != nil {
				return 0, err
			}
		} else {
			return 0, fmt.Errorf("db actor integrity fault: no tx source or source statement")
		}
	}
	var balance uint
	if err := th.balancePlayerStmt.QueryRow(playerId).Scan(&balance); err != nil {
		return 0, err
	}
	return balance, nil
}

func (th *dbactor) addPlayer(playerId string, amount uint) error {
	var err error
	if th.addPlayerStmt == nil {
		if th.txsource != nil && th.txsource.addPlayerStmt != nil && th.tx != nil {
			if th.addPlayerStmt, err = th.txsource.addPlayerStmt.Transaction(th.tx); err != nil {
				return err
			}
		} else {
			return fmt.Errorf("db actor integrity fault: no tx source or source statement")
		}
	}
	_, err = th.addPlayerStmt.Exec(playerId, amount)
	return err
}

func (th *dbactor) fundPlayer(playerId string, amount uint) error {
	var err error
	if th.fundPlayerStmt == nil {
		if th.txsource != nil && th.txsource.fundPlayerStmt != nil && th.tx != nil {
			if th.fundPlayerStmt, err = th.txsource.fundPlayerStmt.Transaction(th.tx); err != nil {
				return err
			}
		} else {
			return fmt.Errorf("db actor integrity fault: no tx source or source statement")
		}
	}
	_, err = th.fundPlayerStmt.Exec(amount, playerId)
	return err
}

func (th *dbactor) takePlayer(playerId string, amount uint) error {
	var err error
	if th.takePlayerStmt == nil {
		if th.txsource != nil && th.txsource.takePlayerStmt != nil && th.tx != nil {
			if th.takePlayerStmt, err = th.txsource.takePlayerStmt.Transaction(th.tx); err != nil {
				return err
			}
		} else {
			return fmt.Errorf("db actor integrity fault: no tx source or source statement")
		}
	}
	_, err = th.takePlayerStmt.Exec(amount, playerId)
	return err
}

func (th *dbactor) announceTournament(tornamentId string, deposit uint) error {
	var err error
	if th.announceTournamentStmt == nil {
		if th.txsource != nil && th.txsource.announceTournamentStmt != nil && th.tx != nil {
			if th.announceTournamentStmt, err = th.txsource.announceTournamentStmt.Transaction(th.tx); err != nil {
				return err
			}
		} else {
			return fmt.Errorf("db actor integrity fault: no tx source or source statement")
		}
	}
	_, err = th.announceTournamentStmt.Exec(tornamentId, deposit)
	return err
}

func (th *dbactor) tournamentState(tornamentId string) (byte, error) {
	var err error
	if th.tournamentStateStmt == nil {
		if th.txsource != nil && th.txsource.tournamentStateStmt != nil && th.tx != nil {
			if th.tournamentStateStmt, err = th.txsource.tournamentStateStmt.Transaction(th.tx); err != nil {
				return byte(0), err
			}
		} else {
			return 0, fmt.Errorf("db actor integrity fault: no tx source or source statement")
		}
	}
	var state string
	if err := th.tournamentStateStmt.QueryRow(tornamentId).Scan(&state); err != nil {
		return byte(0), err
	}
	return state[0], nil
}

func (th *dbactor) getTournament(tornamentId string) (uint, string, error) {
	var err error
	if th.getTournamentStmt == nil {
		if th.txsource != nil && th.txsource.getTournamentStmt != nil && th.tx != nil {
			if th.getTournamentStmt, err = th.txsource.getTournamentStmt.Transaction(th.tx); err != nil {
				return 0, "", err
			}
		} else {
			return 0, "", fmt.Errorf("db actor integrity fault: no tx source or source statement")
		}
	}
	var state string
	var deposit uint
	if err := th.getTournamentStmt.QueryRow(tornamentId).Scan(&deposit, &state); err != nil {
		return 0, "", err
	}
	return deposit, state, nil
}

func (th *dbactor) closeTournament(tornamentId string) error {
	var err error
	if th.closeTournamentStmt == nil {
		if th.txsource != nil && th.txsource.closeTournamentStmt != nil && th.tx != nil {
			if th.closeTournamentStmt, err = th.txsource.closeTournamentStmt.Transaction(th.tx); err != nil {
				return err
			}
		} else {
			return fmt.Errorf("db actor integrity fault: no tx source or source statement")
		}
	}
	_, err = th.closeTournamentStmt.Exec(tornamentId)
	return err
}

func (th *dbactor) joinTournament(tornamentId string, playerId string, joinerType string, bakerId string, stake uint) error {
	var err error
	if th.joinTournamentStmt == nil {
		if th.txsource != nil && th.txsource.joinTournamentStmt != nil && th.tx != nil {
			if th.joinTournamentStmt, err = th.txsource.joinTournamentStmt.Transaction(th.tx); err != nil {
				return err
			}
		} else {
			return fmt.Errorf("db actor integrity fault: no tx source or source statement")
		}
	}
	_, err = th.joinTournamentStmt.Exec(playerId, tornamentId, joinerType, bakerId, stake)
	return err
}

func (th *dbactor) getTournamentPlayerBakers(tornamentId string, playerId string, inject func(linkid uint, playerId string, bakerType string, bakerid string, stake uint) bool) (resErr error) {
	var err error
	if th.getTournamentPlayerBakersStmt == nil {
		if th.txsource != nil && th.txsource.getTournamentPlayerBakersStmt != nil && th.tx != nil {
			if th.getTournamentPlayerBakersStmt, err = th.txsource.getTournamentPlayerBakersStmt.Transaction(th.tx); err != nil {
				return err
			}
		} else {
			return fmt.Errorf("db actor integrity fault: no tx source or source statement")
		}
	}
	var linkid uint
	var currPlayerId string
	var bakerType string
	var bakerid string
	var stake uint
	if rows, err := th.getTournamentPlayerBakersStmt.Query(tornamentId, playerId); err != nil {
		return err
	} else {
		defer func() {
			if rec := recover(); rec != nil {
				resErr = fmt.Errorf("exception in getTournamentPlayerBakers %s", rec)
			}
			if rows != nil {
				if err := rows.Close(); err != nil {
					resErr = err
				}
				if err := rows.Err(); err != nil {
					resErr = err
				}
			}
		}()
		for rows.Next() {
			if err := rows.Scan(&linkid, &currPlayerId, &bakerType, &bakerid, &stake); err != nil {
				return err
			}
			if inject(linkid, currPlayerId, bakerType, bakerid, stake) == false {
				break
			}
		}
		rows = nil
		return err
	}
}

func (th *dbactor) getTournamentTotalStake(tornamentId string) (total uint, err error) {
	if th.getTournamentTotalStakeStmt == nil {
		if th.txsource != nil && th.txsource.getTournamentTotalStakeStmt != nil && th.tx != nil {
			if th.getTournamentTotalStakeStmt, err = th.txsource.getTournamentTotalStakeStmt.Transaction(th.tx); err != nil {
				return 0, err
			}
		} else {
			return 0, fmt.Errorf("db actor integrity fault: no tx source or source statement")
		}
	}
	if err := th.getTournamentTotalStakeStmt.QueryRow(tornamentId).Scan(&total); err != nil {
		return total, err
	}
	return total, nil
}

func (th *dbactor) getTournamentTotalPlayerStake(tornamentId string, playerId string) (total uint, err error) {
	if th.getTournamentTotalPLayerStakeStmt == nil {
		if th.txsource != nil && th.txsource.getTournamentTotalStakeStmt != nil && th.tx != nil {
			if th.getTournamentTotalPLayerStakeStmt, err = th.txsource.getTournamentTotalPLayerStakeStmt.Transaction(th.tx); err != nil {
				return 0, err
			}
		} else {
			return 0, fmt.Errorf("db actor integrity fault: no tx source or source statement")
		}
	}
	if err := th.getTournamentTotalPLayerStakeStmt.QueryRow(tornamentId, playerId).Scan(&total); err != nil {
		return total, err
	}
	return total, nil
}

/*
func (th *dbactor) getTournamentBakers(tornamentId string, inject func(linkid uint, playerId string, bakerType string, bakerid string, stake uint) bool) (resErr error) {
	var err error
	if th.getTournamentBakersStmt == nil {
		if th.txsource != nil && th.txsource.getTournamentBakersStmt != nil && th.tx != nil {
			if th.getTournamentBakersStmt, err = th.txsource.getTournamentBakersStmt.Transaction(th.tx); err != nil {
				return err
			}
		} else {
			return fmt.Errorf("db actor integrity fault: no tx source or source statement")
		}
	}
	var linkid uint
	var bakerType string
	var playerId string
	var bakerid string
	var stake uint
	if rows, err := th.getTournamentBakersStmt.Query(tornamentId); err != nil {
		return err
	} else {
		defer func() {
			if rec := recover(); rec != nil {
				resErr = fmt.Errorf("exception in getTournamentBakers %s", rec)
			}
			if rows != nil {
				if err := rows.Close(); err != nil {
					resErr = err
				}
				if err := rows.Err(); err != nil {
					resErr = err
				}
			}
		}()
		for rows.Next() {
			if err := rows.Scan(&linkid, &playerId, &bakerType, &bakerid, &stake); err != nil {
				return err
			}
			if inject(linkid, playerId, bakerType, bakerid, stake) == false {
				break
			}
		}
		rows = nil
		return err
	}
}
*/
