FROM golang:1.7.6


ENV MYSQL_DATABASE tournament
ENV MYSQL_USER service
ENV MYSQL_PASSWORD some_my_sql_password
ENV MYSQL_PORT 3306
ENV MYSQL_HOST localhost
ENV LISTEN_PORT 8080

RUN go get github.com/go-sql-driver/mysql
ADD *.go /go/src/gitlab.com/DDanchenko/tournament/

WORKDIR /go/src/gitlab.com/DDanchenko/tournament

RUN go build

CMD ./tournament -dbname $MYSQL_DATABASE -dblogin $MYSQL_USER -dbpasswd $MYSQL_PASSWORD -dbport $MYSQL_PORT -dbhost $MYSQL_HOST -port $LISTEN_PORT
# CMD bash
