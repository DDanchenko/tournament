package main

import (
	"database/sql"

	"fmt"

	_ "github.com/go-sql-driver/mysql"
)

type IStatement interface {
	Close() error
	Exec(args ...interface{}) (sql.Result, error)
	Query(args ...interface{}) (*sql.Rows, error)
	QueryRow(args ...interface{}) *sql.Row
	Transaction(tx *sql.Tx) (IStatement, error)
}

type statement struct {
	query string
	db    *sql.DB
	*sql.Stmt
}

func NewStatement(query string, db *sql.DB) (IStatement, error) {
	var err error
	stmt := &statement{query: query, db: db}
	if stmt.Stmt, err = db.Prepare(query); err != nil {
		return nil, err
	} else {
		return stmt, nil
	}
}

func (th *statement) Transaction(tx *sql.Tx) (IStatement, error) {
	var err error
	stmt := &statement{query: th.query, db: th.db}
	if stmt.Stmt, err = tx.Prepare(th.query); err != nil {
		return nil, err
	}
	return stmt, nil
}

func NewTestStatement(query string, db *sql.DB) (IStatement, error) {
	return &teststatement{query: query, db: db}, nil
}

type teststatement struct {
	tx    *sql.Tx
	query string
	db    *sql.DB
}

func (th *teststatement) Close() error {
	return nil
}
func (th *teststatement) Exec(args ...interface{}) (sql.Result, error) {
	if th.tx != nil {
		return th.tx.Exec(th.query, args...)
	}
	return th.db.Exec(th.query, args...)
}
func (th *teststatement) Query(args ...interface{}) (*sql.Rows, error) {
	if th.tx != nil {
		return th.tx.Query(th.query, args...)
	}
	return th.db.Query(th.query, args...)
}
func (th *teststatement) QueryRow(args ...interface{}) *sql.Row {
	if th.tx != nil {
		return th.tx.QueryRow(th.query, args...)
	}
	return th.db.QueryRow(th.query, args...)
}

func (th *teststatement) Transaction(tx *sql.Tx) (IStatement, error) {
	if th.tx != nil {
		return nil, fmt.Errorf("this statement is already created by transaction")
	}
	//fmt.Println(th.query)
	return &teststatement{query: th.query, db: th.db, tx: tx}, nil
}
