curl -i -L -b headers "http://localhost:8080/reset"

# Prepare initial balances
echo "fund?playerId=P1&points=300"
curl -i -L -b headers "http://localhost:8080/fund?playerId=P1&points=300"
echo "take?playerId=P2&points=3000"
curl -i -L -b headers "http://localhost:8080/take?playerId=P1&points=3000"
echo "fund?playerId=P2&points=300"
curl -i -L -b headers "http://localhost:8080/fund?playerId=P2&points=300"
echo "fund?playerId=P3&points=300"
curl -i -L -b headers "http://localhost:8080/fund?playerId=P3&points=300"
echo "fund?playerId=P4&points=500"
curl -i -L -b headers "http://localhost:8080/fund?playerId=P4&points=500"
echo "fund?playerId=P5&points=1000"
curl -i -L -b headers "http://localhost:8080/fund?playerId=P5&points=1000"

echo "http://localhost:8080/balance?playerId=P1&playerId=P2&playerId=P3&playerId=P4&playerId=P5"
curl -i -L -b headers "http://localhost:8080/balance?playerId=P1&playerId=P2&playerId=P3&playerId=P4&playerId=P5"


# Tournament deposit is 1000 points
echo "announceTournament?tournamentId=1&deposit=1000"
curl -i -L -b headers "http://localhost:8080/announceTournament?tournamentId=1&deposit=1000"
# P5 joins on his own
echo "joinTournament?tournamentId=1&playerId=P5"
curl -i -L -b headers "http://localhost:8080/joinTournament?tournamentId=1&playerId=P5"
#P1 joins backed by P2, P3, P4
echo "joinTournament?tournamentId=1&playerId=P1&backerId=P2&backerId=P3&backerId=P4"
curl -i -L -b headers "http://localhost:8080/joinTournament?tournamentId=1&playerId=P1&backerId=P2&backerId=P3&backerId=P4"
# curl  "http://localhost:8080/resultTournament"  POST {"winners": [{"playerId": "P1", "prize": 2000}]}
echo "http://localhost:8080/resultTournament -> POST {'tournamentId': '1','winners': [{'playerId': 'P1', 'prize': 2000}]}"
curl -i -L -b headers -X POST "http://localhost:8080/resultTournament" --header "Content-Type:application/json" -d '{"tournamentId": "1","winners": [{"playerId": "P1", "prize": 2000}]}'


echo "http://localhost:8080/balance?playerId=P1&playerId=P2&playerId=P3&playerId=P4&playerId=P5"
curl -i -L -b headers "http://localhost:8080/balance?playerId=P1&playerId=P2&playerId=P3&playerId=P4&playerId=P5"
