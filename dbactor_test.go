package main

import (
	"database/sql"
	"fmt"
	"testing"

	sqlmock "gopkg.in/DATA-DOG/go-sqlmock.v1"
)

func TestNewDbActor(t *testing.T) {
	db, _, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()

	if _, err := newDbActor(db, NewTestStatement); err != nil {
		t.Errorf("newDBActor init failed")
	}
	if _, err := newDbActor(db, func(query string, db *sql.DB) (IStatement, error) { return nil, fmt.Errorf("test error") }); err == nil {
		t.Errorf("newDBActor not return error")
	}
}

func TestIsPlayerExist(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()

	var actor dbActor
	if actor, err = newDbActor(db, NewTestStatement); err != nil {
		t.Errorf("newDBActor init failed")
	}
	playerTestID := "testplayer"
	playerTest2ID := "testplayer2"
	playerTest3ID := "testplayer3"
	var present bool

	mock.ExpectBegin()
	mock.ExpectQuery("SELECT points FROM players WHERE playerid=").WithArgs(playerTestID).WillReturnRows(sqlmock.NewRows([]string{"points"}).AddRow(uint(101)))
	mock.ExpectQuery("SELECT points FROM players WHERE playerid=").WithArgs(playerTest2ID).WillReturnError(sql.ErrNoRows)
	mock.ExpectQuery("SELECT points FROM players WHERE playerid=").WithArgs(playerTest3ID).WillReturnError(fmt.Errorf("some error"))
	mock.ExpectRollback()
	mock.ExpectBegin()

	actorTrans, err := actor.beginTransaction()
	if actorTrans == nil || err != nil {
		t.Errorf("can't start transaction: %s", err)
	}

	if present, err = actorTrans.isPlayerExist(playerTestID); err != nil || present != true {
		t.Errorf("isPlayerExist with %s fail: %s", playerTestID, err)
	}
	if present, err = actorTrans.isPlayerExist(playerTest2ID); err != sql.ErrNoRows || present != false {
		t.Errorf("isPlayerExist with %s fail: %s", playerTest2ID, err)
	}
	if present, err = actorTrans.isPlayerExist(playerTest3ID); err == nil {
		t.Errorf("isPlayerExist with %s fail: %s", playerTest3ID, err)
	}

	actorTrans.rollback()

	if actorTrans, err = actor.beginTransaction(); err != nil {
		t.Errorf("isPlayerExist with fail: %s", err)
	}
	if dbactorStr, ok := actorTrans.(*dbactor); ok == true {
		dbactorStr.txsource = nil
		dbactorStr.tx = nil
		if _, err = actorTrans.isPlayerExist(playerTestID); err == nil {
			t.Errorf("takePlayer with %s fail: %s", playerTestID, err)
		}
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expections: %s", err)
	}
}

func TestGetPlayerBalance(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()

	var actor dbActor
	if actor, err = newDbActor(db, NewTestStatement); err != nil {
		t.Errorf("newDBActor init failed")
	}
	playerTestID := "testplayer"
	playerTest2ID := "testplayer2"
	playerTest3ID := "testplayer3"
	var balance uint

	mock.ExpectBegin()
	mock.ExpectQuery("SELECT points FROM players WHERE playerid=").WithArgs(playerTestID).WillReturnRows(sqlmock.NewRows([]string{"points"}).AddRow(uint(101)))
	mock.ExpectQuery("SELECT points FROM players WHERE playerid=").WithArgs(playerTest2ID).WillReturnError(sql.ErrNoRows)
	mock.ExpectQuery("SELECT points FROM players WHERE playerid=").WithArgs(playerTest3ID).WillReturnError(fmt.Errorf("some error"))
	mock.ExpectRollback()
	mock.ExpectBegin()

	actorTrans, err := actor.beginTransaction()
	if actorTrans == nil || err != nil {
		t.Errorf("can't start transaction: %s", err)
	}
	if balance, err = actorTrans.getPlayerBalance(playerTestID); err != nil || balance != 101 {
		t.Errorf("getPlayerBalance with %s fail: %s", playerTestID, err)
	}
	if balance, err = actorTrans.getPlayerBalance(playerTest2ID); err != sql.ErrNoRows {
		t.Errorf("getPlayerBalance with %s fail: %s", playerTest2ID, err)
	}
	if balance, err = actorTrans.getPlayerBalance(playerTest3ID); err == nil {
		t.Errorf("getPlayerBalance with %s fail: %s", playerTest3ID, err)
	}
	actorTrans.rollback()

	if actorTrans, err = actor.beginTransaction(); err != nil {
		t.Errorf("getPlayerBalance with fail: %s", err)
	}
	if dbactorStr, ok := actorTrans.(*dbactor); ok == true {
		dbactorStr.txsource = nil
		dbactorStr.tx = nil
		if _, err = actorTrans.getPlayerBalance(playerTestID); err == nil {
			t.Errorf("takePlayer with %s fail: %s", playerTestID, err)
		}
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expections: %s", err)
	}
}

func TestAddPlayer(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()

	var actor dbActor
	if actor, err = newDbActor(db, NewTestStatement); err != nil {
		t.Errorf("newDBActor init failed")
	}
	var amount uint = 101
	playerTestID := "testplayer"
	playerTest2ID := "testplayer2"

	mock.ExpectBegin()
	mock.ExpectExec("INSERT INTO players \\(playerid,points\\) VALUES ").WithArgs(playerTestID, amount).WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectExec("INSERT INTO players \\(playerid,points\\) VALUES ").WithArgs(playerTest2ID, amount).WillReturnError(fmt.Errorf("some error"))
	mock.ExpectRollback()
	mock.ExpectBegin()

	actorTrans, err := actor.beginTransaction()
	if actorTrans == nil || err != nil {
		t.Errorf("can't start transaction: %s", err)
	}

	if err = actorTrans.addPlayer(playerTestID, amount); err != nil {
		t.Errorf("addPlayer with %s fail: %s", playerTestID, err)
	}
	if err = actorTrans.addPlayer(playerTest2ID, amount); err == nil {
		t.Errorf("addPlayer with %s fail: %s", playerTest2ID, err)
	}

	actorTrans.rollback()

	if actorTrans, err = actor.beginTransaction(); err != nil {
		t.Errorf("addPlayer with fail: %s", err)
	}
	if dbactorStr, ok := actorTrans.(*dbactor); ok == true {
		dbactorStr.txsource = nil
		dbactorStr.tx = nil
		if err = actorTrans.addPlayer(playerTestID, amount); err == nil {
			t.Errorf("takePlayer with %s fail: %s", playerTestID, err)
		}
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expections: %s", err)
	}

}

func TestFundPlayer(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()

	var actor dbActor
	if actor, err = newDbActor(db, NewTestStatement); err != nil {
		t.Errorf("newDBActor init failed")
	}
	var amount uint = 101
	playerTestID := "testplayer"
	playerTest2ID := "testplayer2"

	mock.ExpectBegin()
	mock.ExpectExec("UPDATE players SET players.points=players.points").WithArgs(amount, playerTestID).WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectExec("UPDATE players SET players.points=players.points").WithArgs(amount, playerTest2ID).WillReturnError(fmt.Errorf("some error"))
	mock.ExpectRollback()
	mock.ExpectBegin()

	actorTrans, err := actor.beginTransaction()
	if actorTrans == nil || err != nil {
		t.Errorf("can't start transaction: %s", err)
	}

	if err = actorTrans.fundPlayer(playerTestID, amount); err != nil {
		t.Errorf("fundPlayer with %s fail: %s", playerTestID, err)
	}
	if err = actorTrans.fundPlayer(playerTest2ID, amount); err == nil {
		t.Errorf("fundPlayer with %s fail: %s", playerTest2ID, err)
	}

	actorTrans.rollback()

	if actorTrans, err = actor.beginTransaction(); err != nil {
		t.Errorf("fundPlayer with fail: %s", err)
	}
	if dbactorStr, ok := actorTrans.(*dbactor); ok == true {
		dbactorStr.txsource = nil
		dbactorStr.tx = nil
		if err = actorTrans.fundPlayer(playerTestID, amount); err == nil {
			t.Errorf("takePlayer with %s fail: %s", playerTestID, err)
		}
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expections: %s", err)
	}
}

func TestTakePlayer(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()

	var actor dbActor
	if actor, err = newDbActor(db, NewTestStatement); err != nil {
		t.Errorf("newDBActor init failed")
	}
	var amount uint = 101
	playerTestID := "testplayer"
	playerTest2ID := "testplayer2"

	mock.ExpectBegin()
	mock.ExpectExec("UPDATE players SET players.points=players.points").WithArgs(amount, playerTestID).WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectExec("UPDATE players SET players.points=players.points").WithArgs(amount, playerTest2ID).WillReturnError(fmt.Errorf("some error"))
	mock.ExpectRollback()
	mock.ExpectBegin()

	actorTrans, err := actor.beginTransaction()
	if actorTrans == nil || err != nil {
		t.Errorf("can't start transaction: %s", err)
	}

	if err = actorTrans.takePlayer(playerTestID, amount); err != nil {
		t.Errorf("takePlayer with %s fail: %s", playerTestID, err)
	}
	if err = actorTrans.takePlayer(playerTest2ID, amount); err == nil {
		t.Errorf("takePlayer with %s fail: %s", playerTest2ID, err)
	}
	actorTrans.rollback()

	if actorTrans, err = actor.beginTransaction(); err != nil {
		t.Errorf("takePlayer with fail: %s", err)
	}
	if dbactorStr, ok := actorTrans.(*dbactor); ok == true {
		dbactorStr.txsource = nil
		dbactorStr.tx = nil
		if err = actorTrans.takePlayer(playerTestID, amount); err == nil {
			t.Errorf("takePlayer with %s fail: %s", playerTestID, err)
		}
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expections: %s", err)
	}
}

func TestAnnounceTournament(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()

	var actor dbActor
	if actor, err = newDbActor(db, NewTestStatement); err != nil {
		t.Errorf("newDBActor init failed")
	}
	var deposit uint = 101
	tournamentTestID := "tournamenttestid"
	tournamentTest2ID := "tournamenttestid2"

	mock.ExpectBegin()
	mock.ExpectExec("INSERT INTO tournaments \\(idtournaments,deposit,state\\) VALUES ").WithArgs(tournamentTestID, deposit).WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectExec("INSERT INTO tournaments \\(idtournaments,deposit,state\\) VALUES ").WithArgs(tournamentTest2ID, deposit).WillReturnError(fmt.Errorf("some error"))
	mock.ExpectRollback()
	mock.ExpectBegin()

	actorTrans, err := actor.beginTransaction()
	if actorTrans == nil || err != nil {
		t.Errorf("can't start transaction: %s", err)
	}

	if err = actorTrans.announceTournament(tournamentTestID, deposit); err != nil {
		t.Errorf("announceTournament with %s fail: %s", tournamentTestID, err)
	}
	if err = actorTrans.announceTournament(tournamentTest2ID, deposit); err == nil {
		t.Errorf("announceTournament with %s fail: %s", tournamentTest2ID, err)
	}

	actorTrans.rollback()

	if actorTrans, err = actor.beginTransaction(); err != nil {
		t.Errorf("announceTournament with fail: %s", err)
	}
	if dbactorStr, ok := actorTrans.(*dbactor); ok == true {
		dbactorStr.txsource = nil
		dbactorStr.tx = nil
		if err = actorTrans.announceTournament(tournamentTest2ID, deposit); err == nil {
			t.Errorf("announceTournament with %s fail: %s", tournamentTest2ID, err)
		}
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expections: %s", err)
	}
}

func TestTournamentState(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()

	var actor dbActor
	if actor, err = newDbActor(db, NewTestStatement); err != nil {
		t.Errorf("newDBActor init failed")
	}
	var state byte
	tournamentTestID := "tournamenttestid"
	tournamentTest2ID := "tournamenttestid2"
	tournamentTest3ID := "tournamenttestid3"

	mock.ExpectBegin()
	mock.ExpectQuery("SELECT tournaments.state FROM tournaments WHERE tournaments.idtournaments").WithArgs(tournamentTestID).WillReturnRows(sqlmock.NewRows([]string{"points"}).AddRow("c"))
	mock.ExpectQuery("SELECT tournaments.state FROM tournaments WHERE tournaments.idtournaments").WithArgs(tournamentTest2ID).WillReturnError(sql.ErrNoRows)
	mock.ExpectQuery("SELECT tournaments.state FROM tournaments WHERE tournaments.idtournaments").WithArgs(tournamentTest3ID).WillReturnError(fmt.Errorf("some error"))
	mock.ExpectRollback()
	mock.ExpectBegin()

	actorTrans, err := actor.beginTransaction()
	if actorTrans == nil || err != nil {
		t.Errorf("can't start transaction: %s", err)
	}

	if state, err = actorTrans.tournamentState(tournamentTestID); err != nil || state != 'c' {
		t.Errorf("tournamentState with %s fail: %s", tournamentTestID, err)
	}
	if _, err = actorTrans.tournamentState(tournamentTest2ID); err == nil {
		t.Errorf("tournamentState with %s fail: %s", tournamentTest2ID, err)
	}
	if _, err = actorTrans.tournamentState(tournamentTest3ID); err == nil {
		t.Errorf("tournamentState with %s fail: %s", tournamentTest3ID, err)
	}

	actorTrans.rollback()

	if actorTrans, err = actor.beginTransaction(); err != nil {
		t.Errorf("tournamentState with fail: %s", err)
	}
	if dbactorStr, ok := actorTrans.(*dbactor); ok == true {
		dbactorStr.txsource = nil
		dbactorStr.tx = nil
		if _, err = actorTrans.tournamentState(tournamentTest2ID); err == nil {
			t.Errorf("tournamentState with %s fail: %s", tournamentTest2ID, err)
		}
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expections: %s", err)
	}
}

func TestGetTournament(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()

	var actor dbActor
	if actor, err = newDbActor(db, NewTestStatement); err != nil {
		t.Errorf("newDBActor init failed")
	}
	var deposit uint
	var state string
	tournamentTestID := "tournamenttestid"
	tournamentTest2ID := "tournamenttestid2"
	tournamentTest3ID := "tournamenttestid3"

	mock.ExpectBegin()
	mock.ExpectQuery("SELECT tournaments.deposit,tournaments.state FROM tournaments WHERE tournaments.idtournaments=").WithArgs(tournamentTestID).WillReturnRows(sqlmock.NewRows([]string{"deposit", "state"}).AddRow(101, "c"))
	mock.ExpectQuery("SELECT tournaments.deposit,tournaments.state FROM tournaments WHERE tournaments.idtournaments=").WithArgs(tournamentTest2ID).WillReturnError(sql.ErrNoRows)
	mock.ExpectQuery("SELECT tournaments.deposit,tournaments.state FROM tournaments WHERE tournaments.idtournaments=").WithArgs(tournamentTest3ID).WillReturnError(fmt.Errorf("some error"))
	mock.ExpectRollback()
	mock.ExpectBegin()

	actorTrans, err := actor.beginTransaction()
	if actorTrans == nil || err != nil {
		t.Errorf("can't start transaction: %s", err)
	}

	if deposit, state, err = actorTrans.getTournament(tournamentTestID); err != nil || state != "c" || deposit != 101 {
		t.Errorf("getTournament with %s fail: %s", tournamentTestID, err)
	}
	if _, _, err = actorTrans.getTournament(tournamentTest2ID); err == nil {
		t.Errorf("getTournament with %s fail: %s", tournamentTest2ID, err)
	}
	if _, _, err = actorTrans.getTournament(tournamentTest3ID); err == nil {
		t.Errorf("getTournament with %s fail: %s", tournamentTest3ID, err)
	}

	actorTrans.rollback()

	if actorTrans, err = actor.beginTransaction(); err != nil {
		t.Errorf("getTournament with fail: %s", err)
	}
	if dbactorStr, ok := actorTrans.(*dbactor); ok == true {
		dbactorStr.txsource = nil
		dbactorStr.tx = nil
		if _, _, err = actorTrans.getTournament(tournamentTest2ID); err == nil {
			t.Errorf("getTournament with %s fail: %s", tournamentTest2ID, err)
		}
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expections: %s", err)
	}
}

func TestCloseTournament(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()

	var actor dbActor
	if actor, err = newDbActor(db, NewTestStatement); err != nil {
		t.Errorf("newDBActor init failed")
	}
	tournamentTestID := "tournamenttestid"
	tournamentTest2ID := "tournamenttestid2"

	mock.ExpectBegin()
	mock.ExpectExec("UPDATE tournaments SET tournaments.state='c' WHERE  tournaments.idtournaments=").WithArgs(tournamentTestID).WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectExec("UPDATE tournaments SET tournaments.state='c' WHERE  tournaments.idtournaments=").WithArgs(tournamentTest2ID).WillReturnError(fmt.Errorf("some db error"))
	mock.ExpectRollback()
	mock.ExpectBegin()

	actorTrans, err := actor.beginTransaction()
	if actorTrans == nil || err != nil {
		t.Errorf("can't start transaction: %s", err)
	}

	if err = actorTrans.closeTournament(tournamentTestID); err != nil {
		t.Errorf("closeTournament with %s fail: %s", tournamentTestID, err)
	}
	if err = actorTrans.closeTournament(tournamentTest2ID); err == nil {
		t.Errorf("closeTournament with %s fail: %s", tournamentTest2ID, err)
	}

	actorTrans.rollback()

	if actorTrans, err = actor.beginTransaction(); err != nil {
		t.Errorf("closeTournament with fail: %s", err)
	}

	if dbactorStr, ok := actorTrans.(*dbactor); ok == true {
		dbactorStr.txsource = nil
		dbactorStr.tx = nil
		if err = actorTrans.closeTournament(tournamentTestID); err == nil {
			t.Errorf("closeTournament with %s fail: %s", tournamentTest2ID, err)
		}
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expections: %s", err)
	}
}

func TestJoinTournament(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()

	var actor dbActor
	if actor, err = newDbActor(db, NewTestStatement); err != nil {
		t.Errorf("newDBActor init failed")
	}
	tournamentTestID := "t1"
	playerID := "P1"
	joinerType := "b"
	bakerID := "baker2"
	stake := uint(1501)
	tournamentTest2ID := "t2"

	mock.ExpectBegin()
	mock.ExpectExec("INSERT INTO players_in_tournaments \\(playerid,tournamentid,type,bakerid,stake\\) VALUES ").WithArgs(playerID, tournamentTestID, joinerType, bakerID, stake).WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectExec("INSERT INTO players_in_tournaments \\(playerid,tournamentid,type,bakerid,stake\\) VALUES ").WithArgs(playerID, tournamentTest2ID, joinerType, bakerID, stake).WillReturnError(fmt.Errorf("some db error"))
	mock.ExpectRollback()
	mock.ExpectBegin()

	actorTrans, err := actor.beginTransaction()
	if actorTrans == nil || err != nil {
		t.Errorf("can't start transaction: %s", err)
	}

	if err = actorTrans.joinTournament(tournamentTestID, playerID, joinerType, bakerID, stake); err != nil {
		t.Errorf("joinTournament with %s fail: %s", tournamentTestID, err)
	}
	if err = actorTrans.joinTournament(tournamentTest2ID, playerID, joinerType, bakerID, stake); err == nil {
		t.Errorf("joinTournament with %s fail: %s", tournamentTest2ID, err)
	}

	actorTrans.rollback()

	if actorTrans, err = actor.beginTransaction(); err != nil {
		t.Errorf("joinTournament with fail: %s", err)
	}
	if dbactorStr, ok := actorTrans.(*dbactor); ok == true {
		dbactorStr.txsource = nil
		dbactorStr.tx = nil
		if err = actorTrans.joinTournament(tournamentTest2ID, playerID, joinerType, bakerID, stake); err == nil {
			t.Errorf("joinTournament with %s fail: %s", tournamentTest2ID, err)
		}
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expections: %s", err)
	}
}

func TestGetTournamentPlayerBakers(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()

	var actor dbActor
	if actor, err = newDbActor(db, NewTestStatement); err != nil {
		t.Errorf("newDBActor init failed")
	}

	tournamentTestID := "tournamenttestid"
	playerID := "P1"
	tournamentTest2ID := "tournamenttestid2"
	tournamentTest3ID := "tournamenttestid3"

	resultSet := make([]struct {
		linkId    uint
		playerId  string
		bakerType string
		bakerId   string
		stake     uint
	}, 3, 3)

	resultSet[0].linkId = 1
	resultSet[0].playerId = "P1"
	resultSet[0].bakerType = "p"
	resultSet[0].bakerId = ""
	resultSet[0].stake = 1501
	resultSet[1].linkId = 2
	resultSet[1].playerId = "P2"
	resultSet[1].bakerType = "r"
	resultSet[1].bakerId = "baker1"
	resultSet[1].stake = 1502
	resultSet[2].linkId = 3
	resultSet[2].playerId = "P3"
	resultSet[2].bakerType = "p"
	resultSet[2].bakerId = "baker2"
	resultSet[2].stake = 1503

	mock.ExpectBegin()
	mock.ExpectQuery("SELECT linkid,playerid,type,bakerid,stake FROM players_in_tournaments WHERE players_in_tournaments.tournamentid=").WithArgs(tournamentTestID, playerID).WillReturnRows(sqlmock.NewRows([]string{"linkid", "playerid", "type", "bakerid", "stake"}).AddRow(resultSet[0].linkId, resultSet[0].playerId, resultSet[0].bakerType, resultSet[0].bakerId, resultSet[0].stake).AddRow(resultSet[1].linkId, resultSet[1].playerId, resultSet[1].bakerType, resultSet[1].bakerId, resultSet[1].stake).AddRow(resultSet[2].linkId, resultSet[2].playerId, resultSet[2].bakerType, resultSet[2].bakerId, resultSet[2].stake))
	mock.ExpectQuery("SELECT linkid,playerid,type,bakerid,stake FROM players_in_tournaments WHERE players_in_tournaments.tournamentid=").WithArgs(tournamentTest2ID, playerID).WillReturnError(sql.ErrNoRows)
	mock.ExpectQuery("SELECT linkid,playerid,type,bakerid,stake FROM players_in_tournaments WHERE players_in_tournaments.tournamentid=").WithArgs(tournamentTest3ID, playerID).WillReturnError(fmt.Errorf("some error"))
	mock.ExpectRollback()
	mock.ExpectBegin()
	mock.ExpectBegin()

	actorTrans, err := actor.beginTransaction()
	if actorTrans == nil || err != nil {
		t.Errorf("can't start transaction: %s", err)
	}

	resultSetCurr := 0
	testFail := false

	injector := func(linkid uint, playerId string, bakerType string, bakerid string, stake uint) bool {
		if (resultSet[resultSetCurr].linkId != linkid ||
			resultSet[resultSetCurr].playerId != playerId ||
			resultSet[resultSetCurr].bakerType != bakerType ||
			resultSet[resultSetCurr].bakerId != bakerid ||
			resultSet[resultSetCurr].stake != stake) && testFail == false {
			testFail = true
		}
		resultSetCurr++
		return true
	}

	if err = actorTrans.getTournamentPlayerBakers(tournamentTestID, playerID, injector); err != nil || testFail == true {
		t.Errorf("getTournamentPlayerBakers with %s fail: %s", tournamentTestID, err)
	}
	if err = actorTrans.getTournamentPlayerBakers(tournamentTest2ID, playerID, nil); err == nil {
		t.Errorf("getTournamentPlayerBakers with %s fail: %s", tournamentTest2ID, err)
	}
	if err = actorTrans.getTournamentPlayerBakers(tournamentTest3ID, playerID, nil); err == nil {
		t.Errorf("getTournamentPlayerBakers with %s fail: %s", tournamentTest3ID, err)
	}

	actorTrans.rollback()

	actorTrans, err = actor.beginTransaction()
	if actorTrans == nil || err != nil {
		t.Errorf("can't start transaction: %s", err)
	}

	if actorTrans, err = actor.beginTransaction(); err != nil {
		t.Errorf("getTournamentPlayerBakers with fail: %s", err)
	}
	if dbactorStr, ok := actorTrans.(*dbactor); ok == true {
		dbactorStr.txsource = nil
		dbactorStr.tx = nil
		if err = actorTrans.getTournamentPlayerBakers("1", "2", nil); err == nil {
			t.Errorf("getTournament with %s fail: %s", "1", err)
		}
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expections: %s", err)
	}
}

func TestGetTournamentTotalStake(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()

	var actor dbActor
	if actor, err = newDbActor(db, NewTestStatement); err != nil {
		t.Errorf("newDBActor init failed")
	}
	tournamentTestID := "tournamenttestid"
	tournamentTest2ID := "tournamenttestid2"
	tournamentTest3ID := "tournamenttestid3"

	mock.ExpectBegin()
	mock.ExpectQuery("SELECT SUM\\(players_in_tournaments.stake\\) as totalStake FROM players_in_tournaments WHERE players_in_tournaments.tournamentid=").WithArgs(tournamentTestID).WillReturnRows(sqlmock.NewRows([]string{"totalStake"}).AddRow(2051))
	mock.ExpectQuery("SELECT SUM\\(players_in_tournaments.stake\\) as totalStake FROM players_in_tournaments WHERE players_in_tournaments.tournamentid=").WithArgs(tournamentTest2ID).WillReturnError(sql.ErrNoRows)
	mock.ExpectQuery("SELECT SUM\\(players_in_tournaments.stake\\) as totalStake FROM players_in_tournaments WHERE players_in_tournaments.tournamentid=").WithArgs(tournamentTest3ID).WillReturnError(fmt.Errorf("some error"))
	mock.ExpectRollback()
	mock.ExpectBegin()

	actorTrans, err := actor.beginTransaction()
	if actorTrans == nil || err != nil {
		t.Errorf("can't start transaction: %s", err)
	}

	if total, err := actorTrans.getTournamentTotalStake(tournamentTestID); err != nil || total != 2051 {
		t.Errorf("getTournamentPlayerBakers with %s fail: %s", tournamentTestID, err)
	}
	if _, err = actorTrans.getTournamentTotalStake(tournamentTest2ID); err == nil {
		t.Errorf("getTournamentPlayerBakers with %s fail: %s", tournamentTest2ID, err)
	}
	if _, err = actorTrans.getTournamentTotalStake(tournamentTest3ID); err == nil {
		t.Errorf("getTournamentPlayerBakers with %s fail: %s", tournamentTest3ID, err)
	}

	actorTrans.rollback()

	if actorTrans, err = actor.beginTransaction(); err != nil {
		t.Errorf("getTournamentTotalStake with fail: %s", err)
	}
	if dbactorStr2, ok := actorTrans.(*dbactor); ok == true {
		dbactorStr2.txsource = nil
		dbactorStr2.tx = nil
		if _, err = dbactorStr2.getTournamentTotalStake(tournamentTestID); err == nil {
			t.Errorf("getTournament with %s fail: %s", tournamentTest2ID, err)
		}
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expections: %s", err)
	}
}

func TestGetTournamentTotalPlayerStake(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()

	var actor dbActor
	if actor, err = newDbActor(db, NewTestStatement); err != nil {
		t.Errorf("newDBActor init failed")
	}
	tournamentTestID := "tournamenttestid"
	tournamentTest2ID := "tournamenttestid2"
	tournamentTest3ID := "tournamenttestid3"

	mock.ExpectBegin()
	mock.ExpectQuery("SELECT SUM\\(players_in_tournaments.stake\\) as totalStake FROM players_in_tournaments WHERE players_in_tournaments.tournamentid=").WithArgs(tournamentTestID, "P1").WillReturnRows(sqlmock.NewRows([]string{"totalStake"}).AddRow(2051))
	mock.ExpectQuery("SELECT SUM\\(players_in_tournaments.stake\\) as totalStake FROM players_in_tournaments WHERE players_in_tournaments.tournamentid=").WithArgs(tournamentTest2ID, "P1").WillReturnError(sql.ErrNoRows)
	mock.ExpectQuery("SELECT SUM\\(players_in_tournaments.stake\\) as totalStake FROM players_in_tournaments WHERE players_in_tournaments.tournamentid=").WithArgs(tournamentTest3ID, "P1").WillReturnError(fmt.Errorf("some error"))
	mock.ExpectRollback()
	mock.ExpectBegin()

	actorTrans, err := actor.beginTransaction()
	if actorTrans == nil || err != nil {
		t.Errorf("can't start transaction: %s", err)
	}

	if total, err := actorTrans.getTournamentTotalPlayerStake(tournamentTestID, "P1"); err != nil || total != 2051 {
		t.Errorf("getTournamentTotalPlayerStake with %s fail: %s", tournamentTestID, err)
	}
	if _, err = actorTrans.getTournamentTotalPlayerStake(tournamentTest2ID, "P1"); err == nil {
		t.Errorf("getTournamentTotalPlayerStake with %s fail: %s", tournamentTest2ID, err)
	}
	if _, err = actorTrans.getTournamentTotalPlayerStake(tournamentTest3ID, "P1"); err == nil {
		t.Errorf("getTournamentTotalPlayerStake with %s fail: %s", tournamentTest3ID, err)
	}

	actorTrans.rollback()

	if actorTrans, err = actor.beginTransaction(); err != nil {
		t.Errorf("getTournamentTotalPlayerStake with fail: %s", err)
	}
	if dbactorStr, ok := actorTrans.(*dbactor); ok == true {
		dbactorStr.txsource = nil
		dbactorStr.tx = nil
		if _, err = actorTrans.getTournamentTotalPlayerStake(tournamentTest3ID, "P1"); err == nil {
			t.Errorf("getTournament with %s fail: %s", tournamentTest2ID, err)
		}
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expections: %s", err)
	}
}

/*var addPlayerQuery = "INSERT INTO players (playerid,points) VALUES (?,?);"
var balancePlayerQuery = "SELECT points FROM players WHERE playerid=?;"
var fundPlayerQuery = "UPDATE players SET players.points=players.points+? WHERE players.playerid=?;"
var takePlayerQuery = "UPDATE players SET players.points=players.points-? WHERE players.playerid=?;"
var announceTournamentQuery = "INSERT INTO tournaments (idtournaments,deposit,state) VALUES (?,?,'s');"
var closeTournamentQuery = "UPDATE tournaments SET tournaments.state='c' WHERE  tournaments.idtournaments=?;"
var tournamentStateQuery = "SELECT tournaments.state FROM tournaments WHERE tournaments.idtournaments=?;"
var joinTournamentQuery = "INSERT INTO players_in_tournaments (playerid,tournamentid,type,bakerid,stake) VALUES (?,?,?,?,?);"
var getTournamentQuery = "SELECT tournaments.deposit,tournaments.state FROM tournaments WHERE tournaments.idtournaments=?;"
var getTournamentPlayerBakersQuery = "SELECT linkid,playerid,type,bakerid,stake FROM players_in_tournaments WHERE players_in_tournaments.tournamentid=? AND players_in_tournaments.playerid=?;"
var getTournamentTotalStakeQuery =       "SELECT SUM(players_in_tournaments.stake) as totalStake FROM players_in_tournaments WHERE players_in_tournaments.tournamentid=?;"
var getTournamentTotalPLayerStakeQuery = "SELECT SUM(players_in_tournaments.stake) as totalStake FROM players_in_tournaments WHERE players_in_tournaments.tournamentid=? AND  players_in_tournaments.playerid=?;"*/

/*
	-- isPlayerExist(playerId string) (bool, error)
	-- getPlayerBalance(playerId string) (uint, error)
	-- addPlayer(playerId string, amount uint) error
	-- fundPlayer(playerId string, amount uint) error
	-- takePlayer(playerId string, amount uint) error
	-- announceTournament(tornamentId string, deposit uint) error
	-- tournamentState(tornamentId string) (byte, error)
	-- getTournament(tornamentId string) (uint, string, error)
	-- closeTournament(tornamentId string) error
	--joinTournament(tornamentId string, playerId string, joinerType string, bakerId string, stake uint) error
	--getTournamentPlayerBakers(tornamentId string, playerId string, inject func(linkid uint, playerId string, bakerType string, bakerid string, stake uint) bool) (resErr error)
	--getTournamentTotalStake(tornamentId string) (total uint, err error)
	getTournamentTotalPlayerStake(tornamentId string, playerId string) (total uint, err error)
	--getTournamentBakers(tornamentId string, inject func(linkid uint, playerId string, bakerType string, bakerid string, stake uint) bool) (resErr error)
	beginTransaction() (dbTransActor, error)
*/
