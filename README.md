  --help usage

  -dbhost string
    	database hostname (default "localhost")

  -dblogin string
    	database username (default "tournamentservice")

  -dbname string
    	data base name (default "tournament")

  -dbpasswd string
    	database name password (default "tournamentservpwd")

  -dbport int
    	database port (default 3306)

  -port int
    	port (default 8080)

to install use go get:


    go get gitlab.com/DDanchenko/tournament

or use git:


    mkdir $GOPATH/gitlab.com/DDanchenko/tournament
    cd $GOPATH/gitlab.com/DDanchenko/tournament
    git clone https://gitlab.com/DDanchenko/tournament.git


to run containers:


    docker-compose up


to run main scenario:


    bash apptest.sh


docker-compose.yml - pull image from hub.docker.io.

 docker-compose-dev.yml - build image from sources.
